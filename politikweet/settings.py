"""
Django settings for politikweet project on Heroku. For more info, see:
https://github.com/heroku/heroku-django-template

"""

import os
import re
import sys

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.celery import CeleryIntegration
from sentry_sdk.integrations.redis import RedisIntegration

import dj_database_url
import mongoengine
import plotly
import pytz
from celery.schedules import crontab
from django.urls import reverse, reverse_lazy

print('POLITIKWEET.SETTINGS')
SITE_ID = 1

__MONTH_SECONDS__ = 2592000  # of seconds in a month
__HARD_TIME_LIMIT__ = 10800  # 3 hours
__SOFT_TIME_LIMIT__ = 1020  # 17 minutes soft

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv('SECRET_KEY')

# mongo settings
# mongo engine
MONGO_URL = os.getenv('MONGO_URL')
# connect to mongoengine
mongoengine.connect(host=MONGO_URL)

REDIS_URL = os.getenv('REDIS_URL')
ON_HEROKU = os.getenv('ON_HEROKU')
DATABASE_URL = os.getenv('DATABASE_URL')

# SECURITY WARNING: don't run with debug turned on in production!

if ON_HEROKU:
    DEBUG = False
    # force ssl
    SECURE_SSL_REDIRECT = True
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True
    SENTRY_RELEASE = "politikweet@1.0.1"
    SENTRY_ENV = 'PRODUCTION'

    # sentry integration
    # import it but only if we need it

    SENTRY_DSN="https://7a4e3f857cd94bd5b6a35287e7f27990@o184970.ingest.sentry.io/1278796"
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        integrations=[
            DjangoIntegration(),
            CeleryIntegration(),
            RedisIntegration()
        ],
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production,
        traces_sample_rate=.755,

        # If you wish to associate users to errors (assuming you are using
        # django.contrib.auth) you may enable sending PII data.
        send_default_pii=True,

        # By default the SDK will try to use the SENTRY_RELEASE
        # environment variable, or infer a git commit
        # SHA as release, however you may want to set
        # something more human-readable.
        # release="myapp@1.0.0",
        release=SENTRY_RELEASE

    )





# Application definition
LOGIN_REDIRECT_URL = reverse_lazy('secure-dashboard')
LOGIN_URL = reverse_lazy('account_login')
LOGOUT_REDIRECT_URL = reverse_lazy('home')
LOGOUT_ON_GET = True  # from allauth

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.sitemaps',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.admin',
    'django.contrib.humanize',
    'bootstrapform',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.twitter',
    'whitenoise.runserver_nostatic',
    'django.contrib.staticfiles',
    'django_celery_beat',
    'ajax_select',
    'organizations',
    'anymail',
    'crispy_forms',
    'simple_history',
    'django_tables2',
    'django_extensions',
    'rest_framework',
    'cities_light',
    'politikweet',
    'public',
    'secure',
    'core',
    'interrogator',
)

AJAX_SELECT_BOOTSTRAP = True

DISALLOWED_USER_AGENTS = (
    re.compile(r'^NaverBot.*'),
    re.compile(r'^EmailSiphon.*'),
    re.compile(r'^SiteSucker.*'),
    re.compile(r'^Slurp'),
    re.compile(r'^sohu-search'),
)

USE_ETAGS = True
CLIENT_DEFAULT_TIME_ZONE = "America/New_York"
MIDDLEWARE = [
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.http.ConditionalGetMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'simple_history.middleware.HistoryRequestMiddleware',
    'politikweet.middleware.timezone.TimezoneMiddleware',
    'politikweet.middleware.team.TeamMiddleware',
    'politikweet.middleware.social_connect.SocialConnectMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware'
]

ROOT_URLCONF = 'politikweet.urls'

SESSION_ENGINE = 'django.contrib.sessions.backends.db'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'core.context_processors.core'

            ],
            'debug': DEBUG,
        },
    },
]

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',  # Needed to login by username in Django admin, regardless of `allauth`
    'allauth.account.auth_backends.AuthenticationBackend',
    # `allauth` specific authentication methods, such as login by e-mail
)

WSGI_APPLICATION = 'politikweet.wsgi.application'

SESSION_EXPIRE_AT_BROWSER_CLOSE = True


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

ADMINS = [('Henry Adam', 'henryradam@gmail.com'),
          ('Henry Adam', 'henry.adam@politikweet.com')]

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = [
    'politikweet.com',
    'dev.politikweet.com',
    'www.politikweet.com',
    'politikweet.herokuapp.com'
]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

# Extra places for collectstatic to find static files.
STATICFILES_DIRS = [
    os.path.join(PROJECT_ROOT, 'static'),
]

# Simplified static file serving.
# https://warehouse.python.org/project/whitenoise/
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

# databse setup
DATABASES={}
# Parse database configuration from $DATABASE_URL
if os.getenv('CODESHIP_ENV'):
    DATABASES['default'] = {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'development',  # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': os.getenv('PG_USER'),
        'PASSWORD': os.getenv('PG_PASSWORD'),
        'HOST': '127.0.0.1',  # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': ''  # Set to empty string for default.
    }
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
            'LOCATION': os.getenv('REDIS_URL', REDIS_URL),
            'OPTIONS': {
                'DB': 0
            }
        },
        'interrogations': {
            'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
            'LOCATION': os.getenv('REDIS_URL', REDIS_URL),
            'OPTIONS': {
                'DB': 3
            }
        },
        'plots': {
            'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
            'LOCATION': os.getenv('REDIS_URL', REDIS_URL),
            'OPTIONS': {
                'DB': 11
            }
        },
        'tweets': {
            'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
            'LOCATION': os.getenv('REDIS_URL', REDIS_URL),
            'OPTIONS': {
                'DB': 4
            }
        }
    }
elif 'test' in sys.argv:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'default',
            'USER': 'tests',
            'PASSWORD': '123456',
            'HOST': 'db',
            'PORT': 5432,
        }
    }
else:
    DATABASES = {
        'default': dj_database_url.parse(DATABASE_URL)
    }


    CACHES = {
        'default': {
            'BACKEND': 'redis_cache.RedisCache',
            'LOCATION': os.getenv('REDIS_URL'),
            'OPTIONS': {
                'DB': 0
            }
        },
        'interrogations': {
            'BACKEND': 'redis_cache.RedisCache',
            'LOCATION': os.getenv('REDIS_URL'),
            'OPTIONS': {
                'DB': 3
            }
        },
        'plots': {
            'BACKEND': 'redis_cache.RedisCache',
            'LOCATION': os.getenv('REDIS_URL'),
            'OPTIONS': {
                'DB': 11
            }
        },
        'tweets': {
            'BACKEND': 'redis_cache.RedisCache',
            'LOCATION': os.getenv('REDIS_URL'),
            'OPTIONS': {
                'DB': 4
            }
        }
    }

CACHE_MIDDLEWARE_SECONDS = 60
ANYMAIL = {
    "MAILGUN_API_KEY": "key-ad0eeae3266315f157218b2d99323a25",  # 87720325b040582c1b83687e8b2f9135-07bc7b05-f75ca9f1
    "MAILGUN_SENDER_DOMAIN": "mail.politikweet.com"
}
EMAIL_BACKEND = 'anymail.backends.mailgun.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.mailgun.org'
EMAIL_HOST_USER = os.getenv('MAILGUN_SMTP_LOGIN')
EMAIL_HOST_PASSWORD = os.getenv('MAILGUN_SMTP_PASSWORD')
EMAIL_PORT = 587
DEFAULT_FROM_EMAIL = "noreply@politikweet.com"

PLOTLY_USERNAME = os.getenv("PLOTLY_USERNAME")
PLOTLY_APIKEY = os.getenv("PLOTLY_APIKEY")

CRISPY_TEMPLATE_PACK = 'bootstrap3'

KEY_PREFIX = "politikweet"

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True

        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',

        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'cities_light': {
            'handlers': ['console'],
            'propagate': True,
            'level': 'DEBUG',
        },
        # al
    }
}

INTERROGATOR_ABORT_THRESHOLD_IN_SECONDS = 259200 #THREE DAYS IN SECONDS

CELERY_BEAT_SCHEDULER = 'django_celery_beat.schedulers:DatabaseScheduler'
CELERY_BROKER_URL = os.getenv('CLOUDAMQP_URL')

CELERY_RESULT_BACKEND = "{redis_url}".format(redis_url=REDIS_URL)
CELERY_STATE_DB = PROJECT_ROOT + "/celery/state/politikweet-celery-state"

CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_CREATE_MISSING_QUEUES = True
CELERY_SEND_TASK_ERROR_EMAILS = True
# CELERY_TASK_TIME_LIMIT = __HARD_TIME_LIMIT__ # 3 hours
CELERY_TASK_SOFT_TIME_LIMIT = __SOFT_TIME_LIMIT__  # 17mins minutes
# CELERY_TASK_RESULT_EXPIRES = __MONTH_SECONDS__  # 1 month

# # if we dont have any _tasks using the rate limits
# # turn them off as they incurr overhead
# CELERY_DISABLE_RATE_LIMITS = True

# enable utc on celery
CELERY_ENABLE_UTC = True
CELERY_TIME_ZONE = TIME_ZONE
# CELERY_MAX_TASKS_PER_CHILD = 10000
# CELERY_WORKER_CONCURRENCY = 1   # this is the one I was actually looking for
CELERY_WORKER_MAX_MEMORY_PER_CHILD = 12 * 10000  # approx 12 mb

## start celery worker using the prefork pool CPU BOUND
# celery -A politikweet worker -Q celery,tendrilsQ  -l INFO -n tendrilsQ@%h --without-gossip --without-heartbeat
# celery -A politikweet worker -Q celery,interrogatorQ  -l INFO -n interrogatorQ007@%h --without-gossip --without-heartbeat
# celery -A politikweet worker -Q celery,interrogatorQ  -l INFO -n interrogatorQ-%n@%h --without-gossip --without-heartbeat
# celery -A politikweet worker -Q celery,inquisitorQ  -l INFO -n inquisitorQ@%h --without-gossip --without-heartbeat

## start celery worker using the gevent pool
# ~$ celery -A politikweet worker  -n gevent1@%h --pool=gevent --concurrency=500

# start celery worker using the eventlet pool
# ~$ celery -A politikweet worker -Q tendrilsQ -c 100 -P eventlet -l INFO -n eventlet_%n@%h --without-gossip --without-heartbeat

# start a beat worker
# $ celery -A politikweet beat --scheduler django_celery_beat.schedulers:DatabaseScheduler -l INFO


from kombu import Exchange, Queue

# Define a media switch of type Direct Switch
TENDRILS_EXCHANGE = Exchange('tendrilsX', type='direct')
INTERROGATOR_EXCHANGE = Exchange('interrogatorX', type='direct')
INQUISITOR_EXCHANGE = Exchange('inquisitorX', type='direct')

CELERY_QUEUES = (
    Queue(name='interrogatorQ', exchange=INTERROGATOR_EXCHANGE, routing_key='interrogator.tasks.interrogators.interrogate'),
    Queue(name='tendrilsQ', exchange=TENDRILS_EXCHANGE, routing_key='interrogator.tasks.tendrils'),
    Queue(name='inquisitorQ', exchange=INQUISITOR_EXCHANGE, routing_key='interrogator.tasks.inquisitors.inquisitor')
)

CELERY_ROUTES = {
    'core._tasks.generic_immediate': {
        'queue': 'celery'
    },
    'celery.debug_task': {
        'queue': 'celery'
    },
    'interrogator.tasks.inquisitor_scribe_entity_recognition':{
        'queue': 'inquisitorQ',
        'routing_key': 'interrogator.tasks.inquisitor'
    },
    'interrogator.tasks.interrogate': {
        'queue': 'interrogatorQ',
        'routing_key': 'interrogator.tasks.interrogate'

    },
    'interrogator.tasks.inquisitor': {
        'queue': 'inquisitorQ',
        'routing_key': 'interrogator.tasks.inquisitor'
    },
    'interrogator.tasks.extend_tendril': {
        'queue': 'tendrilsQ',
        'routing_key': 'interrogator.tasks.tendrils'
    },
   'interrogator.tasks.extend_tendril_callback': {
        'queue': 'tendrilsQ',
        'routing_key': 'interrogator.tasks.tendrilsCB'
    },

}

SIMPLE_HISTORY_HISTORY_CHANGE_REASON_USE_TEXT_FIELD = True
# application settings

MINIMUM_KEYWORD_FREQUENCY_TO_APPEAR = 15

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    )
}

# ALL Auth
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_AUTHENTICATION_METHOD = 'email'

# CITIES light
CITIES_LIGHT_DATA_DIR = "{}/logs/cities-light/data/".format(BASE_DIR)
CITIES_LIGHT_TRANSLATION_LANGUAGES = ['en']
CITIES_LIGHT_INCLUDE_COUNTRIES = ['US']
CITIES_LIGHT_INCLUDE_CITY_TYPES = [
    'PPL', 'PPLA', 'PPLA2', 'PPLA3', 'PPLA4', 'PPLC', 'PPLF', 'PPLG', 'PPLL', 'PPLR', 'PPLS', 'STLMT',
]
CITIES_LIGHT_APP_NAME = "core"
CITIES_LIGHT_INDEX_SEARCH_NAMES = True

# politkweet twitter access outside of allauth
POLITIKWEET_TWITTER_CONSUMER_KEY = os.getenv("POLITIKWEET_TWITTER_CONSUMER_KEY")
POLITIKWEET_TWITTER_CONSUMER_SECRET = os.getenv("POLITIKWEET_TWITTER_CONSUMER_SECRET")
POLITIKWEET_TWITTER_ACCESS_TOKEN = os.getenv("POLITIKWEET_TWITTER_ACCESS_TOKEN")
POLITIKWEET_TWITTER_ACCESS_TOKEN_SECRET = os.getenv("POLITIKWEET_TWITTER_ACCESS_TOKEN")

SOCIALACCOUNT_STORE_TOKENS=True

from politikweet.utils import randomstring

MOMMY_CUSTOM_FIELDS_GEN = {
    'autoslug.fields.AutoSlugField': lambda: '-'.join(randomstring(lower=3, upper=11).split()),
    'organizations.fields.SlugField': lambda: '-'.join(randomstring(lower=3, upper=11).split())
}
DEFAULT_INTERROGATION_UNIT_SAMPLE_COUNT = 100
DJANGO_TABLES2_TEMPLATE = """django_tables2/bootstrap-responsive.html"""
# overwrite if local

SHORT_DATETIME_FORMAT='m/d/Y H:i:s'


try:
    if 'test' in sys.argv:
        from politikweet.settings_test_local import *
    else:
        from politikweet.settings_local_overwrite import *
except ImportError as e:
    print(e)
    pass

