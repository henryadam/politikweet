# Create your views here.

import tweepy
import random
import sentimentacron
import nltk
import re
import mongoengine
import os
import time
import requests
from bs4 import BeautifulSoup


from mongoengine import DynamicDocument, DateTimeField,StringField
import datetime
import numpy as np
from urllib.parse import urlparse


class Base(DynamicDocument):

    """Base superclass for all models"""
    created_date = DateTimeField(default=datetime.datetime.utcnow())
    meta = {'allow_inheritance': True}

class Sentimentalite(Base):
    """dynamic document describing an analysis unit"""
    member_of = StringField(max_length=50)

class AnalysisUnit(Base):
    member_of = StringField(max_length=50)

class Tweetstream(DynamicDocument):
    created_date = DateTimeField(default=datetime.datetime.utcnow())
    meta = {'allow_inheritance': True}


# mongo engine
MONGO_HOST = 'ds041861.mongolab.com'
MONGO_PORT = 41861
MONGO_DATABASE = "criticalitude"
MONGO_USERNAME = "jack"
MONGO_PASSWORD = "44326373333ere56dfaza"

# connect to mongoengine

_MONGODB_DATABASE_HOST = \
    'mongodb://%s:%s@%s/%s' \
    % (MONGO_USERNAME, MONGO_PASSWORD, MONGO_HOST, MONGO_DATABASE)

#mongoengine.connect(MONGO_DATABASE, host="mongodb://politikweet-www:44326373333ere56dfaza@politikweet-shard-00-00.geybn.mongodb.net:27017,politikweet-shard-00-01.geybn.mongodb.net:27017,politikweet-shard-00-02.geybn.mongodb.net:27017/criticalitude?ssl=true&replicaSet=atlas-qv1y8x-shard-0&authSource=admin&retryWrites=true&w=majority")

CRITICALITUDE_KEY_WORDS = [
    "mike pence",
    "rex tillerson",
    "steven mnuchin",
    "james mattis",
    "jeff sessions",
    "sonny purdue",
    "andre puzder",
    "elaine chao",
    "betsy devos",
    "rick perry",
    "david shulkin",
    "reince priebus",
    "scott pruitt",
    "ben carson",
    "john kelly",
    "ryan zinke",
    "donald trump"
    "congress",
    "senate",
    "house of representatives",
    "congressman",
    "senator",
    "white house",
]

TWITTER_CONSUMER_KEY = "xhtpkD4JV7qlyizDGe0tLg"
TWITTER_CONSUMER_SECRET = "STO0YHWjcFdHEQlPC8A8va1ZPjLUshZPFJxsnOasvA"
TWITTER_ACCESS_TOKEN = "39590750-2Z0C9AC3wozhcsjgACYUfVlTama85T2UbbD1vraOd"
TWITTER_ACCESS_TOKEN_SECRET = "p9IPiJuiEJhzgXP8XiuC6aKIdqPXxiHxREf1T1PJ6a4"

NEWS_ORG_API = '7deb02e1d96b4f35bc5574a0bfab4fab'

auth = tweepy.OAuthHandler(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET)
auth.set_access_token(TWITTER_ACCESS_TOKEN, TWITTER_ACCESS_TOKEN_SECRET)

API = tweepy.API(auth_handler=auth)
SENTI = sentimentacron.Sentimentalyzer()

class MyStreamListener(tweepy.StreamListener):
    def on_connect(self):
        print("Connected")

    def on_error(self, status_code):
        if status_code == 420:
            # returning False in on_data disconnects the stream
            raise Exception(msg="on error")

    def on_exception(self, exception):
        """Called when an unhandled exception occurs."""
        print(repr(exception))
        raise exception

    def on_status(self, status):
        """Called when a new status arrives"""
        key_words = []

        if (not status.retweeted) and ('RT @' not in status.text):
            print()
            print("{} - {}".format("*" * 66, datetime.datetime.utcnow().isoformat()))

            tweet_text = status.text
            tweet_id = status.id

            tokens = nltk.word_tokenize(tweet_text)
            tokens = nltk.pos_tag(tokens)


            print(tokens)
            regex_hashtags = re.compile("#(?:\w+)")
            regex_mentions = re.compile("@(?:\w+)")
            regex_url = re.compile("http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+")
            hash_tags = regex_hashtags.findall(tweet_text)
            print(hash_tags)

            mentions = regex_mentions.findall(tweet_text)
            print(mentions)
            urls = regex_url.findall(tweet_text)


            tweet_text_nourl = re.sub(r"http\S+", "", tweet_text)
            tweet_for_analysis = re.findall(r'\w+', tweet_text_nourl)
            tweet_for_analysis = " ".join(tweet_for_analysis)
            tweet_for_analysis = tweet_for_analysis + "."

            print("{}".format(tweet_for_analysis))

            # well be looking for NN NNS NNP
            for tuple in tokens:

                if tuple[0] not in ["http","https"] and len(tuple[0]) > 4 and (
                                    tuple[1] in ["NN","NS","NNP","NNS","JJ","RB","VBD"]  ):
                    key_words.append(tuple[0])

            key_words.extend(hash_tags)
            key_words.extend(mentions)
            key_words = list(set([ zxz for zxz in key_words if not zxz.startswith("//")]))

            print("::::::::::::::::::::::::::::::::::::::::::: KEYWORDS : {}".format(repr(key_words)))

            sentiment_score = SENTI.sentiment_analysis(tweet_for_analysis)

            data = {
                'compound': sentiment_score['compound']
            }

            absolute_sentiment = SENTI.absolute_sentiment(data)

            print("{} {}".format(sentiment_score, absolute_sentiment))

            tweet_payload = {
                "raw": tweet_text,
                "tokenized": tweet_for_analysis,

            }

            geo = status.geo
            print(geo)
            # here lets follow the links one level out
            embd_url_payload = {}
            each_sentence_score = []
            if urls:
                try:
                    response = requests.get(urls[0], allow_redirects=True,timeout=5 )
                    embd_url_payload['url'] = response.url
                except Exception as e:
                    response = None

                print("status_code",getattr(response,'status_code',None))
                print("redirect url", getattr(response, 'url', None))


                find_this = urlparse(response.url).geturl()

                found_it = Sentimentalite.objects.filter(embedded_url_payload__url__icontains=response.url)
                print("*** ALREADY HAVE URL *** {}".format(bool(found_it)))

                if response and not found_it and response.status_code == 200 and "twitter" not in response.url and not response.url.endswith('pdf') and (
                                "log out" not in response.text.lower() or
                                "authenticate" not in response.text.lower() or
                                "terms of service" not in response.text.lower() or
                                "privacy policy" not in response.text.lower or
                                "login" not in response.text.lower
                ):


                                        # lets soup up the content
                    soup = BeautifulSoup(response.text, 'lxml')

                    paragraphs = soup.find_all('p')



                    text = str()
                    for paragraph in paragraphs:
                        text += ''.join(' '.join(paragraph.get_text().strip().split()))

                    sentences = nltk.sent_tokenize(text)

                    print("Sentences ************* {}".format(len(sentences)))

                    # must have this many sentences to calculate a proper sample
                    if len(sentences) > 10:
                        print(":::::: AND WERE SAVING THIS :::::: {} ::::::::".format(not bool(found_it)))
                        for sentence in sentences:
                            print("****",sentence,"****")
                            sentence_sentiment_score = SENTI.sentiment_analysis(sentence)
                            if sentence_sentiment_score['compound'] != 0.0:
                                each_sentence_score.append(sentence_sentiment_score)

                        embd_url_payload['title'] = soup.title.get_text()
                        embd_url_payload['avg_analysis'] = {
                            "compound": sum(item['compound'] for item in each_sentence_score)/float(len(each_sentence_score)),
                            "positive_score":sum(item['positive_score'] for item in each_sentence_score)/float(len(each_sentence_score)),
                            "negative_score":sum(item['negative_score'] for item in each_sentence_score)/float(len(each_sentence_score)),
                            "sentences_analyzed": len(sentences),

                        }

                        print(float(len(each_sentence_score)))
                        print([item['compound'] for item in each_sentence_score])
                        print(embd_url_payload['avg_analysis'])
                        # now lets a look for news stories based on our tweet

                        save_this = Sentimentalite()
                        save_this.sentiment_score = sentiment_score
                        save_this.tweet_id = tweet_id
                        save_this.absolute_sentiment = absolute_sentiment
                        save_this.key_words = key_words
                        save_this.tweet_payload = tweet_payload
                        save_this.geo = geo
                        save_this.embedded_url_payload = embd_url_payload
                        save_this.sentences_score = each_sentence_score


                        save_this.save()
                    else:
                        print("::::::::::::::::::::::::::::::::::::::::::: NOT LONG ENOUGH")
            else:
                print(":::::::::::::::::::::::::::::::::::::::::::NO EMBEDDED URL ::: SAVE TWEET")
                save_this = Tweetstream()
                save_this.sentiment_score = sentiment_score
                save_this.tweet_id = tweet_id
                save_this.absolute_sentiment = absolute_sentiment
                save_this.key_words = key_words
                save_this.tweet_payload = tweet_payload
                save_this.geo = geo
                save_this.embedded_url_payload = embd_url_payload
                save_this.sentences_score = each_sentence_score

                save_this.save()

def main():
    myStreamListener = MyStreamListener()
    myStream = tweepy.Stream(auth=API.auth, listener=myStreamListener)
    stop = False
    while True:
        try:


            myStream.filter(track=[
                """mike pence,
                rex tillerson,
                steven mnuchin,
                james mattis,
                jeff sessions,
                sonny purdue,
                andre puzder,
                elaine chao,
                betsy devos,
                rick perry,
                david shulkin,
                reince priebus,
                scott pruitt,
                ben carson,
                john kelly,
                ryan zinke,
                donald trump,
                congress,
                senate,
                house of representatives,
                congressman,
                senator,
                white house,
                stephen miller"""
            ],languages=['en'])
        except (KeyboardInterrupt):
            stop = True
            print('Stopping Twitter Streaming Client')
        except Exception as e:
            print('Some exception occurred. Retrying')
            continue

        if stop:
            break

        return None

if __name__ == '__main__':
        main()


