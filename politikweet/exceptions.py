
class CannotDeriveTeam(Exception):
    """
    raised when the app cannot derive a team for the authenticated user
    """

    def __init__(self,  message="Cannot derive Team (organization) for authenticated user"):
        """
        display yhr appropriate message
        @param message:
        """
        self.message = message
        super().__init__(self.message)