# Create your views here.

import tweepy
import random
import sentimentacron

import re
import mongoengine
import os
import time


from mongoengine import DynamicDocument, DateTimeField,StringField
import datetime

class Base(DynamicDocument):

    """Base superclass for all models"""
    created_date = DateTimeField(default=datetime.datetime.utcnow())
    meta = {'allow_inheritance': True}

class Sentimentalite(Base):
    """dynamic document describing an analysis unit"""
    member_of = StringField(max_length=50)

class AnalysisUnit(Base):
    member_of = StringField(max_length=50)


# mongo engine
MONGO_HOST = 'ds041861.mongolab.com'
MONGO_PORT = 41861
MONGO_DATABASE = "criticalitude"
MONGO_USERNAME = "jack"
MONGO_PASSWORD = "44326373333ere56dfaza"

# connect to mongoengine

MONGODB_DATABASE_HOST = \
    'mongodb://%s:%s@%s/%s' \
    % (MONGO_USERNAME, MONGO_PASSWORD, MONGO_HOST, MONGO_DATABASE)

#mongoengine.connect(MONGO_DATABASE, host="mongodb://politikweet-www:44326373333ere56dfaza@politikweet-shard-00-00.geybn.mongodb.net:27017,politikweet-shard-00-01.geybn.mongodb.net:27017,politikweet-shard-00-02.geybn.mongodb.net:27017/criticalitude?ssl=true&replicaSet=atlas-qv1y8x-shard-0&authSource=admin&retryWrites=true&w=majority")

CRITICALITUDE_KEY_WORDS = [
    "mike pence",
    "rex tillerson",
    "steven mnuchin",
    "james mattis",
    "jeff sessions",
    "sonny purdue",
    "andre puzder",
    "elaine chao",
    "betsy devos",
    "rick perry",
    "david shulkin",
    "reince priebus",
    "scott pruitt",
    "ben carson",
    "john kelly",
    "ryan zinke",
    "donald trump"
    "congress",
    "senate",
    "house of representatives",
    "congressman",
    "senator",
    "white house",
]

TWITTER_CONSUMER_KEY = "xhtpkD4JV7qlyizDGe0tLg"
TWITTER_CONSUMER_SECRET = "STO0YHWjcFdHEQlPC8A8va1ZPjLUshZPFJxsnOasvA"
TWITTER_ACCESS_TOKEN = "39590750-2Z0C9AC3wozhcsjgACYUfVlTama85T2UbbD1vraOd"
TWITTER_ACCESS_TOKEN_SECRET = "p9IPiJuiEJhzgXP8XiuC6aKIdqPXxiHxREf1T1PJ6a4"

auth = tweepy.OAuthHandler(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET)
auth.set_access_token(TWITTER_ACCESS_TOKEN, TWITTER_ACCESS_TOKEN_SECRET)

API = tweepy.API(auth_handler=auth)


class MyStreamListener(tweepy.StreamListener):
    def on_connect(self):
        print("Connected")

    def on_error(self, status_code):
        if status_code == 420:
            # returning False in on_data disconnects the stream
            return False

    def on_exception(self, exception):
        """Called when an unhandled exception occurs."""
        print(repr(exception))



    def on_status(self, status):
        """Called when a new status arrives"""
        key_words = []
        if (not status.retweeted) and ('RT @' not in status.text):
            print()
            print("{} - {}".format("*" * 66, datetime.datetime.utcnow().isoformat()))

            tweet_text = status.text
            tweet_id = status.id

            tokens = nltk.word_tokenize(tweet_text)
            tokens = nltk.pos_tag(tokens)
            regex_hashtags = re.compile("#(?:\w+)")
            regex_mentions = re.compile("@(?:\w+)")
            hash_tags = regex_hashtags.findall(tweet_text)
            mentions = regex_mentions.findall(tweet_text)

            tweet_text_nourl = re.sub(r"http\S+", "", tweet_text)


            tweet_for_analysis = re.findall(r'\w+', tweet_text_nourl)
            tweet_for_analysis = " ".join(tweet_for_analysis)
            tweet_for_analysis = tweet_for_analysis + "."

            print("{}".format(tweet_for_analysis))

            # well be looking for NN NNS NNP
            for tuple in tokens:
                if tuple[0] != "http" and len(tuple[0]) > 5 and (
                                    tuple[1] == "NN" or tuple[1] == "NS" or tuple[1] == "NNP" or tuple[1] == "NNS"  ):
                    key_words.append(tuple[0])

            key_words.extend(hash_tags)
            key_words.extend(mentions)
            key_words = list(set([ zxz for zxz in key_words if not zxz.startswith("//")]))

            sentiment_score = sentimentacron.sentimentalyzer().sentiment_analysis(tweet_for_analysis)

            data = {
                'compound': sentiment_score['compound']
            }

            absolute_sentiment = sentimentacron.sentimentalyzer().absolute_sentiment(data)

            print("{} {}".format(sentiment_score, absolute_sentiment))

            tweet_payload = {
                "raw": tweet_text,
                "tokenized": tweet_for_analysis,

            }
            geo = status.geo
            save_this = Sentimentalite()

            save_this.sentiment_score = sentiment_score
            save_this.tweet_id = tweet_id
            save_this.absolute_sentiment = absolute_sentiment
            save_this.key_words = key_words
            save_this.tweet_payload = tweet_payload
            save_this.geo = geo

            save_this.save()
            print("*" * 66)

def main():
    try:

        myStreamListener = MyStreamListener()
        myStream = tweepy.Stream(auth=API.auth, listener=myStreamListener)

        myStream.filter(track=[
            """mike pence,
            rex tillerson,
            steven mnuchin,
            james mattis,
            jeff sessions,
            sonny purdue,
            andre puzder,
            elaine chao,
            betsy devos,
            rick perry,
            david shulkin,
            reince priebus,
            scott pruitt,
            ben carson,
            john kelly,
            ryan zinke,
            donald trump
            congress,
            senate,
            house of representatives,
            congressman,
            senator,
            white house,
            Jim Acosta,
            Christiane Amanpour,
            Chris Cuomo,
            Ana Navarro,
            Hilary Rosen,
            Jeffrey Lord,
            Van Jones"""
        ],languages=['en'])
    except (KeyboardInterrupt, SystemExit):
        print('Stopping Twitter Streaming Client')


if __name__ == '__main__':
    main()
