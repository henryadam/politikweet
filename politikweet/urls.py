"""politikweet URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/{{ docs_version }}/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from __future__ import absolute_import, unicode_literals, print_function

from django.conf import settings
from django.conf.urls import include, re_path
from django.contrib import admin
from organizations.backends import invitation_backend

from public.views import HomeView, TagCloudView, TagCloudDetailView, TagCloudJSONView, PolitikalSentiment, \
    TagCloudUrlDetailsView, AboutView, PrivacyView, TermsView
from secure.views import LogoutSecureView

urlpatterns = [
    re_path(r'^$', HomeView.as_view(), name='home'),
    re_path(r'^about/$', AboutView.as_view(), name='about'),
    re_path(r'^privacy/$', PrivacyView.as_view(), name='privacy'),
    re_path(r'^terms/$', TermsView.as_view(), name='terms'),
    re_path(r'^tag-cloud/$', TagCloudView.as_view(), name='tag-cloud'),
    re_path(r'^tag-cloud-detail/(?P<search_term>.+)/$', TagCloudDetailView.as_view(), name='tag-cloud-term-detail'),
    re_path(r'^tag-cloud-generate/$', TagCloudJSONView.as_view(), name='tag-cloud-generate'),
    re_path(r'^politikal-sentiment/$', PolitikalSentiment.as_view(), name='politikal-sentiment'),
    re_path(r'^tag-cloud-url-detail/(?P<search_term>.+)/$',
            TagCloudUrlDetailsView.as_view(),
            name='tag-cloud-url-detail'
    ),
    re_path(r'^core/', include('core.urls')),
    re_path(r'^secure/', include('secure.urls')),
    re_path(r'^secure/interrogator/', include('interrogator.urls')),
    re_path(r"^accounts/logout/", LogoutSecureView.as_view(), name="account_logout"),
    re_path(r'^accounts/', include('allauth.urls')),
    re_path(r'^organizations/', include('organizations.urls')),
    re_path(r'^invitations/', include(invitation_backend().get_urls())),
    re_path(r'^api-auth/', include('rest_framework.urls')),
    re_path(r'^politikweet-administration/', admin.site.urls)
]

if settings.DEBUG:
    import debug_toolbar
    from django.conf.urls.static import static

    urlpatterns += [
                       re_path(r'^__debug__/', include(debug_toolbar.urls)),

                   ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
