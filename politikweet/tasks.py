from __future__ import absolute_import
import random
from celery import Task
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)


class PolitikweetBaseTask(Task):
    """
        this is the base class that very other task will inherit from
        in this way we can define similar behaviour across all our _tasks.
    """
    abstract = True
    soft_time_limit = 900
    time_limit=900*5

    # set up the logger
    logger = logger

    def run(self, *args, **kwargs):
        super(PolitikweetBaseTask, self).run(args, kwargs)

    def on_retry(self, exc, task_id, args, kwargs, einfo):
        """Log retries to file."""
        #logger.info("on_retry TASK [ {} ] CALLBACK ".format(task_id))
        super(PolitikweetBaseTask, self).on_retry(exc, task_id, args, kwargs, einfo)

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        """Log failures to file."""
        logger.info("on_failure TASK [ {} ] FAILED CALLBACK ".format(task_id))
        super(PolitikweetBaseTask, self).on_failure(exc, task_id, args, kwargs, einfo)

    def on_success(self, retval, task_id, args, kwargs):
        """Log success messages to file."""
        #logger.info("on_success TASK [ {} ] SUCCESS CALLBACK ".format(task_id))
        super(PolitikweetBaseTask, self).on_success(retval, task_id, args, kwargs)

    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        """Log acceptance of task. method overwrite for null use"""
        pass


class AsyncSendMail(PolitikweetBaseTask):
    # the purpose of this task is to provide
    # views a way to take advantage of celery
    # when sending email.

    def run(self, template_path, tolist, from_email=None, data=None, attachments=None, request=None):
        # lets use a exponential retry so that it's backed off
        try:
            pass
        except Exception as e:
            # as a rule lets retry if theres an error lets back off and add jitter max retries is set globally at three
            self.retry(exc=e, countdown=int(random.uniform(2, 4) ** self.request.retries))