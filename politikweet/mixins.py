from __future__ import absolute_import, unicode_literals, print_function

import logging

from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.mixins import AccessMixin
from django.http import JsonResponse
from django.utils.decorators import method_decorator

from secure.utils.user_security_helpers import is_librarian, is_interrogator, is_interlocutor, is_supplicant

logger = logging.getLogger(__name__)


class IsAuthenticatedIsLibrarianAccessMixin(AccessMixin):
    permission_denied_message = "You are not A Librarian or you lack permissions"


    @method_decorator(user_passes_test(is_librarian))
    @method_decorator(user_passes_test(is_interrogator))
    @method_decorator(user_passes_test(is_interlocutor))
    @method_decorator(user_passes_test(is_supplicant))
    # an interrogator is a user that belongs to the Interrogator group and is authenticated
    def dispatch(self, request, *args, **kwargs):
        return super(IsAuthenticatedIsLibrarianAccessMixin, self).dispatch(request, *args, **kwargs)


    def get_permission_denied_message(self):
        """
        Override this method to override the permission_denied_message attribute.
        """
        return self.permission_denied_message


class IsAuthenticatedIsInterrogatorAccessMixin(AccessMixin):
    permission_denied_message = "You are not Interrogator or you lack permissions"


    @method_decorator(user_passes_test(is_interrogator))
    @method_decorator(user_passes_test(is_interlocutor))
    @method_decorator(user_passes_test(is_supplicant))
    # an interrogator is a user that belongs to the Interrogator group and is authenticated

    def get_permission_denied_message(self):
        """
        Override this method to override the permission_denied_message attribute.
        """
        return self.permission_denied_message


class IsAuthenticatedIsInterlocutorAccessMixin(AccessMixin):
    permission_denied_message = "You are not an Interlocutor or you lack permissions"


    @method_decorator(user_passes_test(is_interlocutor))
    @method_decorator(user_passes_test(is_supplicant))
    # an interrogator is a user that belongs to the Interrogator group and is authenticated
    def dispatch(self, request, *args, **kwargs):
        return super(IsAuthenticatedIsInterlocutorAccessMixin, self).dispatch(request, *args, **kwargs)


    def get_permission_denied_message(self):
        """
        Override this method to override the permission_denied_message attribute.
        """
        return self.permission_denied_message


class IsAuthenticatedIsSupplicantAccessMixin(AccessMixin):
    permission_denied_message = "You are not a Supplicant or you lack permissions"


    @method_decorator(user_passes_test(is_supplicant))
    # an interrogator is a user that belongs to the Interrogator group and is authenticated
    def dispatch(self, request, *args, **kwargs):
        return super(IsAuthenticatedIsSupplicantAccessMixin, self).dispatch(request, *args, **kwargs)


    def get_permission_denied_message(self):
        """
        Override this method to override the permission_denied_message attribute.
        """
        return self.permission_denied_message


class JSONResponseMixin(object):
    """
    A mixin that can be used to render a JSON response.
    """


    def render_to_json_response(self, context, **response_kwargs):
        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        return JsonResponse(
            self.get_data(context),
            **response_kwargs
        )


    def get_data(self, context):
        """
        Returns an object that will be serialized as JSON by json.dumps().
        """
        # Note: This is *EXTREMELY* naive; in reality, you'll need
        # to do much more complex handling to ensure that arbitrary
        # objects -- such as Django model instances or querysets
        # -- can be serialized as JSON.
        return context
