import datetime
import random
import string
import urllib.parse
import numpy as np
import pytz
from decimal import Decimal
import itertools
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.db.models import Q, F

#numpy.linspace
_SENTIMENT_RANGES_ = {

    'sentiment_very_satisfied': range(800,1000),#np.arange(Decimal(0.8), Decimal(1.0)),
    'sentiment_satisfied': range(100,800),# np.arange(Decimal(0.2), Decimal(0.8)),
    'sentiment_dissatisfied': range(-100,-600),#np.arange(Decimal(-0.2), Decimal(-0.6)),
    'sentiment_very_dissatisfied': range(-600,-900),# np.arange(Decimal(-0.6), Decimal(-0.9)),
    'sentiment_extremely_dissatisfied': range(-900,-1000),# np.arange(Decimal(-0.9), Decimal(-1.0)),
}

def random_wobble(lmb=2, lmt=4, xlt=1, xtt=3):
    return random.uniform(lmb, lmt) ** random.uniform(xlt, xtt)


def urlencode(str):
    return urllib.parse.quote(str)


def urldecode(str):
    return urllib.parse.unquote(str)


def normalize(values, bounds):
    """
    bounds = {
                'actual': {
                    'lower': round(math.sqrt(lower_limit_actual), 4),
                    'upper': round(math.sqrt(upper_limit_actual), 4)
                },
                'desired': {
                    'lower': 1, 'upper': 6
                }
            }

            normalized_df=(df-df.mean())/df.std()
    """

    return [
        bounds['desired']['lower'] + (x - bounds['actual']['lower']) * (
            bounds['desired']['upper'] - bounds['desired']['lower']) / (
                    bounds['actual']['upper'] - bounds['actual']['lower']) for x in values
    ]


def days_back90():
    utc_now = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)
    return utc_now + datetime.timedelta(days=-90)


def days_back30():
    utc_now = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)
    return utc_now + datetime.timedelta(days=-30)


def randomstring(lower=3, upper=4):
    return ''.join(
        random.choice(
            string.ascii_uppercase
        ) + random.choice(
            string.digits
        ) + random.choice(
            (' ', '')) for _ in range(random.randrange(lower, upper))
    )


def as_percent(numerator=1, denominator=1):
    percentage = round(((float(numerator) / float(denominator)) * 100.00), 0)
    return "{}%".format(percentage)


def draw_pagination():
    pass


def human_sentiment(sentiment):
    """
    'sentiment_very_satisfied': range(800,1000),#np.arange(Decimal(0.8), Decimal(1.0)),
    'sentiment_satisfied': range(100,800),# np.arange(Decimal(0.2), Decimal(0.8)),
    'sentiment_dissatisfied': range(-100,-600),#np.arange(Decimal(-0.2), Decimal(-0.6)),
    'sentiment_very_dissatisfied': range(-600,-900),# np.arange(Decimal(-0.6), Decimal(-0.9)),
    'sentiment_extremely_dissatisfied': range(-900,-1000),# np.arange(Decimal(-0.9), Decimal(-1.0)),
    """
    if -1.0 <= sentiment <= -0.8:
        _style = 'sentiment_extremely_dissatisfied:trending negative'
    elif -0.8 <= sentiment <= -0.6:
        _style = 'sentiment_very_dissatisfied:trending negative'
    elif -0.5 <= sentiment < -0.05:
        _style = "sentiment_dissatisfied:trending negative"
    elif -0.05 <= sentiment <= 0.05:
        _style = "sentiment_neutral:flat"
    elif 0.05 <= sentiment <= 0.6:
        _style = "sentiment_satisfied:trending positive"
    elif 0.6 <= sentiment <= 1.0:
        _style = "sentiment_very_satisfied:trending positive"
    else:
        _style = "sentiment_neutral:ambiguous"

    return _style

def model_to_dict(instance, fields=None, exclude=None, date_to_strf=None):
    from django.db.models.fields.related import ManyToManyField
    from django.db.models.fields import DateTimeField
    from django.db.models.fields.files import ImageField, FileField
    opts = instance._meta
    data = {}

    """
    Why is `__fields` in here?
        it holds the list of fields except for the one ends with a suffix '__[field_name]'.
        When converting a model object to a dictionary using this method,
        You can use a suffix to point to the field of ManyToManyField in the model instance.
        The suffix ends with '__[field_name]' like 'publications__name'
    """
    __fields = list(map(lambda a: a.split('__')[0], fields or []))

    for f in itertools.chain(opts.concrete_fields, opts.virtual_fields, opts.many_to_many):
        is_edtiable = getattr(f, 'editable', False)

        if fields and f.name not in __fields:
            continue

        if exclude and f.name in exclude:
            continue

        if isinstance(f, ManyToManyField):
            if instance.pk is None:
                data[f.name] = []
            else:
                qs = f.value_from_object(instance)
                if qs._result_cache is not None:
                    data[f.name] = [item.pk for item in qs]
                else:
                    try:
                        m2m_field  = list(filter(lambda a: f.name in a and a.find('__') != -1, fields))[0]
                        key = m2m_field[len(f.name) + 2:]
                        data[f.name] = list(qs.values_list(key, flat=True))
                    except IndexError:
                        data[f.name] = list(qs.values_list('pk', flat=True))

        elif isinstance(f, DateTimeField):
            date = f.value_from_object(instance)
            data[f.name] = date_to_strf(date) if date_to_strf else date_to_timestamp(date)

        elif isinstance(f, ImageField):
            image = f.value_from_object(instance)
            data[f.name] = image.url if image else None

        elif isinstance(f, FileField):
            file = f.value_from_object(instance)
            data[f.name] = file.url if file  else None

        elif is_edtiable:
            data[f.name] = f.value_from_object(instance)

    """
    Just call an instance's function or property from a string with the function name in `__fields` arguments.
    """
    funcs = set(__fields) - set(list(data.keys()))
    for func in funcs:
        obj = getattr(instance, func)
        if inspect.ismethod(obj):
            data[func] = obj()
        else:
            data[func] = obj
    return data


def render_human_sentiment(record):
    """
    produce a display
    record in this shape {"sentiment_style":<>,"sentiment":<num>}
    """
    style = record.get('sentiment_style').split(':')
    _h = style[0].split('_').pop()
    trend=None

    if record['sentiment'] < 0:
        trend = "trending down"
    elif record['sentiment'] > 0:
        trend = "trending up"


    temp = f"""
               <div class='row'>
                    <div class='col-sm-12 col-md-6 text-right'>
                        <span class='material-symbols-sharp gi-4x'>{style[0]}</span>
                    </div>
                    <div class='col-sm-12 col-md-6 text-left'>
                        <h5>{_h} {record['sentiment']} - {trend}</h5>
                    </div>
                </div>                        
    """

    return mark_safe(format_html(temp))