"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
from __future__ import absolute_import, unicode_literals, print_function

from model_mommy import mommy
from interrogator.models import Interrogation, InterrogationUnit
from django.contrib.sites.models import Site
from core.models import City, Team
from django.test import TestCase, RequestFactory, Client
from allauth.socialaccount.models import SocialAccount, SocialApp, SocialToken

class PolitikweetBaseTestCase(TestCase):

    def setUp(self):
        print('PolitikweetBaseTestCase')
        self.user = mommy.make('auth.User')
        self.team = mommy.make(Team)
        self.city = mommy.make(City)
        self.site = mommy.make(Site, provider="twitter")
        self.interrogatory = mommy.make(Interrogation, user=self.user)
        self.units= mommy.make(InterrogationUnit, interrogation=self.interrogatory, _quantity=100)
