from django.conf import settings
from core.models import  Team

class TeamMiddleware:
    """
    middle ware is concerned with setting a team Id (organization) it mutates the request object
    and adds an attribute named team_id
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        """
        @param request:
        @return: self.response

        on an authenticated user that does not have request.team_id, look up the first one.
        """
        if not request.user.is_authenticated:
            return self.get_response(request)

        if request.user.is_authenticated and not hasattr(request,'team'):
            request.team = request.user.organizations_organization.all().order_by('-pk').first()
        return self.get_response(request)