from django.conf import settings
from core.models import  Team
from allauth.socialaccount.models import SocialAccount, SocialApp, SocialToken
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

class SocialConnectMiddleware:
    """
    middle ware is concerned with setting a team Id (organization) it mutates the request object
    and adds an attribute named team_id
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        """
        @param request:
        @return: self.response

        on an authenticated user that does not have request.team_id, look up the first one.
        """
        if  not request.user.is_authenticated:
            return self.get_response(request)

        is_connected_to_twitter = SocialAccount.objects.filter(
            user=request.user,
            provider="twitter",
        ).exists()

        if not is_connected_to_twitter:
            messages.add_message(request, messages.INFO, 'Please connect your twitter account.')


        return self.get_response(request)