import os

from celery import Celery
from django.conf import settings

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'politikweet.settings')

app = Celery('politikweet')

app.conf.update(
    broker_pool_limit=1,  # Will decrease connection usage
    broker_heartbeat=None,  # We're using TCP keep-alive instead
    # If you tasks are CPU bound, then limit to the number of cores, otherwise increase substainally
    task_queue_max_priority=10
)
# Using a string here means the worker doesn celery -A politikweet worker -Q tendrilsQ,celery -c 100 -P eventlet -l INFO -n eventlet1@%h --without-gossip --without-heartbeat't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task(bind=True, priority=10, ignore_result=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
