![A Murder of Crows](./politikweet/static/img/murder_of_crows.png "crows")
# **Politikweet** 
######_gain emotional (political) insight from the written (Twitter) word_

**at the intersection of politics and twitter?...cut through the clutter**

Inevitably Twitter and politics are connected, 
at the intersection is **Politikweet**, there is an abundance 
of tweets with embedded articles, **Politikweet** is interested in studying the
sentiment of these embedded articles and 
assigning a sentiment value to hashtags or mentions related to the embedded article.

>###Listen to the Twitter stream... 
>
>- inspect tweets with embedded URL's, 
>- analyze the tweet for sentiment using Python's NLTK, 
>- then follow the link to the content and analyze for sentiment the embedded URLs. 


This then allows **Politikweet** to assign sentiment to a specific hashtag or mention.

---

#About **Politikweet**

This project started as a way to practice with pythons NLTK but with the last presidential election and the current president's love of this medium, it has morphed into a much greater analysis tool. I am looking to gather tweets not written by the people below, but more about the people below.

The main idea here is to get a feeling for how the twitter-verse feels about the United States government, to do this politikweet tracks the keywords below in the public twitter stream capturing tweets, politikweet will then follow any embedded links and analyze for sentiment, both the tweet and any underlying content; the funny thing is that content can be written with positive sentiment on despicable subjects.

This is an exercise in Natural Language Processing. So that we can gain emotional insights from the written word.

_Twitter can tell us a lot about how the world feels about the current administration and has become a bell weather for tracking reputation, so lets track the key words below and see how the twitter-verse feels about our government._

###Keywords tracking in the Twitter stream

>President
Congress,
Senate,
White House,
Supreme Court,
Politics,
Political,
Demagogue,
House Of Representatives,
Department Of Agriculture,
Department Of Commerce,
Department Of Defense,
Department Of Education,
Department Of Health And Human Services,
Department Of Homeland Security,
Department Of Housing And Urban Development,
Department Of Energy


