from __future__ import absolute_import
import random
from celery.utils.log import get_task_logger
from politikweet.celery import app
from politikweet.tasks import PolitikweetBaseTask

logger = get_task_logger(__name__)


@app.task(bind=True, base=PolitikweetBaseTask)
def generic_immediate(self):
    try:
        logger.info("this is an immediate task be on the low priority queue worker")
    except Exception as e:
        # as a rule lets retry if theres an error lets back off and add jitter max retries is set globally at three
        self.retry(exc=e, countdown=int(random.uniform(2, 4) ** self.request.retries))


