import plotly
import plotly.express as px

def sparkline(_df_):
    fig = px.scatter(
        _df_,
        y="value",
        height=50,
        trendline='ols',
        trendline_color_override="#00ff00",

    )
    fig.data[0].update(mode='markers+lines')
    fig.update_traces(
        overwrite=True,
        selector=dict(type="scatter", mode='markers+lines'),
        marker_size=1,
        marker_color="#f73a00",
        line=dict(
            width=1,
            simplify=True,
            shape="spline"
        )
    )
    fig.update_xaxes(
        visible=False,
        type='date',
    )
    fig.update_yaxes(
        visible=False,
        range=[-1, 1],
        fixedrange=True,
        showdividers=True,
    )
    fig.update_layout(
        annotations=[],
        showlegend=False,
        font=dict(
            family='Urbanist',
            size=12,
            color='#000000'
        )
    )

    fig_as_html = plotly.io.to_html(fig, full_html=False, config={
        'displayModeBar': False,
    })

    return fig_as_html