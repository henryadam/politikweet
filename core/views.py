from __future__ import absolute_import, unicode_literals, print_function
import pytz
from django.contrib.auth.models import User, Group
from django.shortcuts import redirect, render
from django.views.generic import TemplateView
from rest_framework import viewsets

from core.serializers import UserSerializer, GroupSerializer


class SetTimezone(TemplateView):

    def get(self, request, *args, **kwargs):
        return render(request, 'core/timezone_set.html', {'timezones': pytz.common_timezones})

    def post(self, request, *args, **kwargs):
        request.session['django_timezone'] = request.POST.get('timezone')
        return redirect('/')


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
