"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
from django.contrib.auth.models import Group, Permission, User
from django.test import TransactionTestCase, RequestFactory, Client
from django.test import override_settings
from django.urls import reverse
from model_mommy import mommy
from django.conf import settings
from django.contrib.auth.models import User, Group
from django.core import mail
from django.urls import reverse
from django.test import TestCase, override_settings, RequestFactory
from faker import Faker
from core.models import Team, City


from .models import Profile





@override_settings(CELERY_ALWAYS_EAGER=True, SITE_ID=1, CELERY_EAGER_PROPAGATES_EXCEPTIONS=True, )
class CoreTestCase(TestCase):

    def setUp(self):
        """
        set uip core test
        """
        fake = Faker()
        fake.seed_instance(6)
        self.registering_email = fake.unique.email()
        self.email_for_create = fake.unique.email()
        self.created_username = fake.unique.user_name()
        self.factory = RequestFactory()

        self.user = User.objects.create_user(self.created_username, self.email_for_create, 'qwerty')

        self.user.first_name = fake.first_name()
        self.user.last_name = fake.last_name()
        self.user.email= self.email_for_create

        self.organization_name = fake.company()

        self.client = Client()
        self.rf = RequestFactory()

        self.cities = mommy.make(
            City, _quantity=50
        )

        self.team = mommy.make(Team)

        self.team.add_user(self.user)

        all_perms = Permission.objects.all()

        interrogator_auth_group, created = Group.objects.get_or_create(
            name="Interrogator"
        )

        librarian_auth_group, created = Group.objects.get_or_create(
            name="Librarian"
        )

        interlocutor_auth_group, created = Group.objects.get_or_create(
            name="Interlocutor"
        )

        supplicant_auth_group, created = Group.objects.get_or_create(
            name="Supplicant"
        )

        librarian_auth_group.permissions.add(*all_perms)

        self.user.groups.add(interrogator_auth_group)
        self.user.groups.add(librarian_auth_group)
        self.user.groups.add(interlocutor_auth_group)
        self.user.groups.add(supplicant_auth_group)

        self.user.save()

        self.client.login(username=self.created_username, password='qwerty')

        self._social_app = mommy.make('SocialApp', provider="twitter")

        self.assertEqual(self.user.is_authenticated, True)


        self.user.save()

    def test_login(self):
        """
        Tes logging in a user
        """
        response = self.client.post(
            reverse('account_login'),
            data={
                "username": self.created_username,
                "password": 'qwerty'}
            , follow=True
        )
        self.assertTrue(self.user.is_authenticated)
        self.assertEqual(response.status_code, 200)

    def test_registration(self):
        """
        register a user
        :return:
        """

        response = self.client.post(
            reverse('account_signup'),
            data={
                "email": self.registering_email,
                "password1": 'qqw#33462543624563456',
                "password2": 'qqw#33462543624563456',
                "organization_title":''}
            , follow=True
        )

        # import pprint
        # pprint.pprint(response.__dict__)
        # look up the user we just created

        registrant = response.wsgi_request.user

        group = Group.objects.filter(name='Librarian').first()

        # # we should end up with a user in the librarian group
        self.assertIsNotNone(group)
        self.assertIn(group, registrant.groups.all())
        self.assertIsNotNone(registrant.organizations_organization.all())
        self.assertTrue(registrant.is_authenticated)
        self.assertEqual(response.status_code, 200)

    def test_send_email(self):
        """
        test sending an email
        """
        mail.send_mail(
            'Subject here', 'Here is the message.',
            'henry.adam@politikweet.com', ['henry.adam@politikweet.com'],
            fail_silently=False,
        )

        # Test that one message has been sent.
        self.assertEqual(len(mail.outbox), 1)

        # Verify that the subject of the first message is correct.
        self.assertEqual(mail.outbox[0].subject, 'Subject here')

    def test_celery_in_eager_mode(self):
        """
        test make sure celery is in eager mode
        """
        self.assertEquals(
            getattr(
                settings, 'CELERY_ALWAYS_EAGER', False)
            , True)
        self.assertEquals(
            getattr(
                settings,
                'CELERY_EAGER_PROPAGATES_EXCEPTIONS', False)
            , True)

    def test_profile_created(self):
        """
        test profile created
        """
        profile_exists = Profile.objects.filter(user=self.user).exists()
        self.assertTrue(profile_exists)
