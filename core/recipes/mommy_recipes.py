import random
import uuid

from django.contrib.auth.models import User, Group
from faker import Faker
from model_mommy.recipe import related, Recipe, foreign_key
from core.models import Profile

user = Recipe(
    User,
    first_name=Faker(['EN_us', 'en_CA', 'de_DE']).first_name(),
    last_name=Faker(['EN_us', 'en_CA', 'de_DE']).last_name(),
    email=Faker(['EN_us', 'en_CA', 'de_DE']).ascii_email(),
    password=Faker(['EN_us', 'en_CA', 'de_DE']).password(
        length=random.randrange(14, 55),
        special_chars=True,
        digits=True,
        upper_case=True,
        lower_case=True)
    ,
    groups=related(),
    is_superuser=random.choice([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0]),
    is_staff=random.choice([1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0]),
    is_active=True,
    profile=foreign_key(Recipe(Profile))

)
