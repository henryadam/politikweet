from __future__ import absolute_import, unicode_literals, print_function
from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin
from core.models import Profile
# Register your models here.

admin.site.register(Profile, SimpleHistoryAdmin)
