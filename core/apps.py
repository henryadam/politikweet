from django.apps import AppConfig
from django.apps import apps


class CoreConfig(AppConfig):
    name = 'core'

    def ready(self):
        from django.contrib.auth.models import Permission
        from core.receivers import user_signed_up_receiver

        Permission.__str__ = lambda self: '{} {} {}'.format(
            self.content_type.app_label,
            self.content_type,
            self.name,
        )


