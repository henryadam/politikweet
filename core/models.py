# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals, print_function

from cities_light.abstract_models import (AbstractCity, AbstractRegion, AbstractCountry, AbstractSubRegion)
from cities_light.receivers import connect_default_signals
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.functional import cached_property
from organizations.models import Organization, OrganizationUser, OrganizationOwner
from simple_history.models import HistoricalRecords


class Profile(models.Model):
    NOAFFILIATION = 0
    DEMOCRAT = 1
    REPUBLICAN = 2
    INDEPENDENT = 3
    LIBERTARIAN = 4
    SOCIAL_DEMOCRAT = 5

    POLITICAL_AFFILIATION_CHOICES = (
        (NOAFFILIATION, 'No Affiliation'),
        (DEMOCRAT, 'Democrat'),
        (REPUBLICAN, 'Republican'),
        (INDEPENDENT, 'Independent'),
        (LIBERTARIAN, 'Libertarian'),
        (SOCIAL_DEMOCRAT, 'Social Democrat')
    )
    user = models.OneToOneField('auth.User', related_name="profile", on_delete=models.CASCADE, db_index=True)
    first_name = models.CharField(max_length=100, blank=True, null=True, db_index=True)
    last_name = models.CharField(max_length=100, blank=True, null=True, db_index=True)
    bio = models.TextField(max_length=500, blank=True, null=True, db_index=True)
    location = models.CharField(max_length=30, blank=True, null=True, )
    birth_date = models.DateField(null=True, blank=True, db_index=True)
    image = models.URLField(null=True, blank=True)
    political_affiliation = models.PositiveSmallIntegerField(
        choices=POLITICAL_AFFILIATION_CHOICES,
        null=True,
        blank=True,
        db_index=True
    )
    changed_by = models.ForeignKey(
        'auth.User',
        on_delete=models.CASCADE,
        related_name="changed_by",
        null=True,
        blank=True
    )

    # maintained by django-simple-history
    history = HistoricalRecords()

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        app_label = "core"

    @cached_property
    def full_name(self):
        """
        cache the full namew
        @return:
        """
        return "{} {}".format(getattr(self, 'first_name', "Please provide First Name"),
                              getattr(self, 'last_name', "Please provide Last Name"))

    def __str__(self):  # __unicode__ for Python 2
        """
        describes what the administrator prints for an object
        @return:
        """
        return u'[%(cls)s:%(id)s] %(username)s %(name)s %(email)s %(is_active)s' % {
            'cls': self.__class__.__name__,
            'id': str(self.id),
            'username': getattr(self.user, 'username', None),
            'name': self.full_name,
            'email': getattr(self.user, 'email', None),
            'is_active': getattr(self.user, 'is_active', None)
        }


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


# organization Models
class TeamOwner(OrganizationOwner):
    """who owns the team"""

    class Meta:
        proxy = True
        verbose_name="general"
        verbose_name_plural="generals"



class Team(Organization):
    """The team, a logical collection of users"""

    class Meta:
        proxy = True
        verbose_name="army"
        verbose_name_plural="armies"


class TeamMember(OrganizationUser):
    """a member of the team"""

    class Meta:
        proxy = True
        verbose_name="soldier"
        verbose_name_plural="soldiers"

# cities light models
class Country(AbstractCountry):
    pass


connect_default_signals(Country)


class Region(AbstractRegion):
    pass


connect_default_signals(Region)


class SubRegion(AbstractSubRegion):
    pass


connect_default_signals(SubRegion)


class City(AbstractCity):
    timezone = models.CharField(max_length=140)
    display_name = models.CharField(max_length=2500)


connect_default_signals(City)


