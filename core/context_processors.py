from __future__ import absolute_import, unicode_literals, print_function

from django.conf import settings


def core(request):
    return {'debug': settings.DEBUG}
