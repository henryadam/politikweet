import logging
import uuid

from allauth.account.signals import user_signed_up
from django.contrib.auth.models import User, Permission, Group
from django.dispatch import receiver
from organizations.utils import create_organization
import secure.utils.user_security_helpers
from core.models import Team

logger = logging.getLogger(__name__)


@receiver(user_signed_up, sender=User, dispatch_uid=uuid.uuid4())
def user_signed_up_receiver(sender, **kwargs):
    """
        This Signal Handler takes care to
    """
    from collections import OrderedDict
    from faker import Faker
    fake = Faker()
    # Creates and seeds a unique `random.Random` object for
    # each internal generator of this `Faker` instance
    fake.seed_instance(49)
    _user = kwargs.get('user')
    _request = kwargs.get('request')
    _post = _request.POST.get('organization_title', None) or fake.unique.company()

    organization = create_organization(
        user=_user,
        name="{}".format(_post),
        org_model=Team,
        is_active=True
    )
    _user.refresh_from_db()
    grp = Group.objects.get(name="Librarian")
    _user.username = _user.email # make sure the username is the email they used
    _user.groups.add(grp.pk) # make sure the registering user is a librartian
    _user.save()
    logger.warning('user-registration')
    return organization
