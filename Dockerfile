# The first instruction is what image we want to base our container on
# We Use an official Python runtime as a parent image
# to remove docker
#Stop the container(s) using the following command:
#     docker-compose down
#Delete all containers using the following command:
#     docker rm -f $(docker ps -a -q)
#Delete all volumes using the following command:
#     docker volume rm $(docker volume ls -q)
   #  docker image rm -f $(docker image ls -q)
#Restart the containers using the following command:
#     docker-compose up -d

# docker-entrypoint-initdb.d/*

FROM python:3.9
# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update -y
RUN apt install -y netcat apt-utils supervisor sudo bash
RUN apt upgrade -y
RUN which nc
RUN rm -rf /var/lib/apt/lists/*

RUN mkdir -p /politikweet/logs/
RUN mkdir -p /politikweet/staticfiles/

WORKDIR /politikweet/
COPY /config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN touch /var/run/supervisor.sock

RUN ln -s /politikweet/config/develop/politikweet.celery.conf /etc/supervisor/conf.d/politikweet.celery.config.conf

#RUN /bin/bash/sudo supervisorctl reread
#RUN /bin/bash/sudo supervisorctl update
#RUN /bin/bash/sudo service supervisor restart
#RUN /bin/bash/sleep 15
#RUN /bin/bash/sudo supervisorctl status all

ENV PYTHONPATH="${PYTHONPATH}:/home/celery/.local/bin"
ENV PATH="${PATH}:/home/celery/.local/bin"
ARG CELERY_USER=celery
RUN sleep 5
RUN useradd -m -U celery
RUN usermod -aG sudo celery

ADD . /politikweet/


RUN chmod +x /usr/local/bin/
RUN chown -R celery:celery .

RUN chmod 777 /politikweet/entrypoint.sh \
    && ln -s /politikweet/entrypoint.sh /


USER celery
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN /usr/local/bin/python -m pip install -r /politikweet/requirements.txt --no-cache-dir



ENTRYPOINT ["/politikweet/entrypoint.sh"]







