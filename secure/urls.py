"""politikweet URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/{{ docs_version }}/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from __future__ import absolute_import, unicode_literals, print_function
from django.conf.urls import url
from secure.views import DashboardSecureView, ProfileSecureView
from django.contrib.auth.decorators import login_required, permission_required
from django.conf.urls import include, url
from django.views import generic


urlpatterns = [
    url(r'^$', login_required(
        DashboardSecureView.as_view()
    ), name='secure-dashboard'),
    url(r'^profile/$', login_required(
        ProfileSecureView.as_view()
    ), name='secure-profile'),
    url(r'^interrogate/', include('interrogator.urls'))
]
