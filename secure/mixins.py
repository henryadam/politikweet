from django.contrib.auth.mixins import  AccessMixin
from secure.utils.user_security_helpers import is_interrogator
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import user_passes_test