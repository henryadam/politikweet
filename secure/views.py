import pandas
import plotly
import plotly.express as px
import plotly.graph_objects as go
import pytz
from allauth.account.views import LogoutView
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Count, Avg
from django.db.models.functions import TruncMinute
from django.views.generic import TemplateView
from organizations.views.mixins import OrganizationMixin
from plotly.subplots import make_subplots

from interrogator.models import Interrogation, InterrogationUnit
from politikweet.mixins import IsAuthenticatedIsSupplicantAccessMixin


class DashboardSecureView(LoginRequiredMixin, TemplateView, OrganizationMixin, IsAuthenticatedIsSupplicantAccessMixin):
    template_name = "secure/dashboard.html"
    permission_denied_message = "you are not allowed here, permission denied"


    def heatmap(self, interrogators, *args, **kwargs):
        """
        generate the heatmap across all interrogators
        :param interrogators: a QUERYSET OF INTERROGATORS THAA BELONG TO THIS USER
        :param args:
        :param kwargs:
        :return:a plotly html figure
        """
        interrogation_units = InterrogationUnit.objects.filter(
            date_completed__isnull=False,
            interrogation__in=interrogators
        ).annotate(
            date=TruncMinute('date_completed',tzinfo=pytz.UTC)
        ).values(
            "date"
        ).annotate(
            created_count=Count('id')
        ).annotate(
            critical_sentiment=Avg('compound'),
        ).order_by("-date")

        df = pandas.DataFrame(interrogation_units, columns=['date', 'created_count', 'critical_sentiment'])

        fig = px.density_heatmap(
            df,
            x='date',
            z='critical_sentiment',
            y='created_count',
            histfunc="avg",
            range_color=[-1, 1],
            color_continuous_scale=['red', 'yellow', 'green'],

        )

        fig = fig.update_layout(
            coloraxis_colorbar=dict(
                title="Sentiment Polarity",
                showticklabels=True,
                ticktext=['(-1) most negative', '(0) nuetral', '(1) most positive'],
                tickvals=[-1, 0, 1],
                ticks="outside",
                ticklabelposition='outside bottom',
                xpad=10,
                ypad=10,

            ),
            annotations=[],
            paper_bgcolor='#183e5f',
            plot_bgcolor='#b1b1b1',
            overwrite=True,
            margin=dict(t=16, l=9, b=16, r=9),
            yaxis_title="Acquisition Frequency",
            xaxis_title="Dates",
            autosize=True,
            font=dict(
                family='Ubuntu',
                size=14,
                color='#ffffff'
            ))

        fig_as_html = plotly.io.to_html(fig, full_html=False, config={'displayModeBar': False})

        return fig_as_html


    def dispatch(self, request, *args, **kwargs):
        return super(DashboardSecureView, self).dispatch(request, *args, **kwargs)


    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data()
        interrogations = Interrogation.objects.filter(
            user=self.request.user
        ).order_by('date_completed','keyword')
        context['figure'] = self.heatmap(interrogations)
        context['interrogations'] = list(interrogations)

        return context


class ProfileSecureView(LoginRequiredMixin, TemplateView, IsAuthenticatedIsSupplicantAccessMixin):
    template_name = "secure/profile.html"


    def dispatch(self, request, *args, **kwargs):
        return super(ProfileSecureView, self).dispatch(request, *args, **kwargs)


class LogoutSecureView(LoginRequiredMixin, LogoutView, IsAuthenticatedIsSupplicantAccessMixin):
    def dispatch(self, request, *args, **kwargs):
        clear_storage = messages.get_messages(request)
        clear_storage.used = True
        if settings.LOGOUT_ON_GET:
            return self.post(*args, **kwargs)
        return super(LogoutSecureView, self).dispatch(request, *args, **kwargs)
