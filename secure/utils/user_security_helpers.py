from __future__ import absolute_import, unicode_literals, print_function
from django.core.exceptions import PermissionDenied

__INTERROGATOR_GROUP__ = 'Interrogator'
__SUPPLICANT_GROUP__ = 'Supplicant'
__INTERLOCUTOR_GROUP__ = 'Interlocutor'
__LIBRARIAN_GROUP__ = 'Librarian'

# the idea here is that the more groups youre in the more permissions you have
__is_librarian__ = {
    __LIBRARIAN_GROUP__,
    __INTERROGATOR_GROUP__,
    __INTERLOCUTOR_GROUP__,
    __SUPPLICANT_GROUP__,
}

__is_interrogator__ = {
    __INTERROGATOR_GROUP__,
    __INTERLOCUTOR_GROUP__,
    __SUPPLICANT_GROUP__,
}

__is_interlocutor__ = {
    __INTERLOCUTOR_GROUP__,
    __SUPPLICANT_GROUP__,
}

__is_supplicant__ = {
    __SUPPLICANT_GROUP__,
}

def is_librarian(user):
    """
    the user belongs to the group Librarians or is a superuser
    :param user:
    :return:
    """
    if user.is_superuser or (user.is_authenticated and user.groups.filter(
            name__in=__is_librarian__).exists()):
        return True
    raise PermissionDenied('You are not a Librarian, nor are you in a parent group.')

def is_interrogator(user):
    """
    the user belongs to the group interrogators or is a superuser
    :param user:
    :return:
    """
    if (user.is_superuser) or is_librarian(user) or (user.is_authenticated and user.groups.filter(
            name__in=__is_interrogator__).exists()):
        return True
    raise PermissionDenied('You are not an Interrogator, nor are you in a parent group.')

def is_interlocutor(user):
    """
    the user belongs to the group Interlocutors or is a superuser
    Interlocutors have Read Permissions to every model
    :param user:
    :return:
    """
    if user.is_superuser or (user.is_authenticated and user.groups.filter(
            name__in=__is_interlocutor__).exists()):
        return True
    raise PermissionDenied('You are not a Interlocutor, nor are you in a parent group.')

def is_supplicant(user):
    """
    the user belongs to the group supplicant, supplicants are the lowest user level

    :param user:
    :return:
    """
    if user.is_superuser or is_interrogator(user) or is_librarian(user) or (user.is_authenticated and user.groups.filter(
            name__in=__is_supplicant__).exists()):
        return True
    raise PermissionDenied('You are not a Supplicant')














