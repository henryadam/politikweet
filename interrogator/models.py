from __future__ import absolute_import, unicode_literals, print_function

import datetime
import logging
import uuid
from numpy import mean
import pytz
import math
from celery import chord
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.db.models import F, Manager
from django.db.models import Q
from django.utils.functional import cached_property
from simple_history.models import HistoricalRecords

from core.models import City, Team, TeamMember
from interrogator.enums import InterrogationHumanPrediction
from interrogator.tasks import extend_tendril, extend_tendril_callback
from django.contrib import admin
# Get an instance of a logger
logger = logging.getLogger(__name__)





class CustomManagerActiveInterrogator(Manager):
    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(is_active=True)  # or whatever your condition is
        return qs

    def all_objects(self):
        return super().get_queryset()



class Interrogation(models.Model):
    """
      describes a single unit of search work
    """
    sample_size = models.IntegerField(
        default=settings.DEFAULT_INTERROGATION_UNIT_SAMPLE_COUNT,
        verbose_name="Size"

    )

    user = models.ForeignKey(
        'auth.User',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name="interrogations")

    team = models.ForeignKey(
        'core.Team',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name="interrogations")
    is_locked = models.BooleanField(default=False)
    is_complete = models.BooleanField(default=False)
    must_include_url = models.BooleanField(default=False)
    keyword = models.CharField(max_length=1000,verbose_name="Keyword Phrase")
    as_typed= models.CharField(max_length=1000, blank=True, null=True)
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    uuid = models.UUIDField(default=uuid.uuid4, db_index=True)
    date_registered = models.DateTimeField(blank=True, null=True, auto_now_add=True, db_index=True)
    date_completed = models.DateTimeField(blank=True, null=True, db_index=True)
    duration_in_seconds=models.IntegerField(null=True, blank=0)
    retract_tendrils_sentiment = models.FloatField(null=True, default=0.0, db_index=True)
    human_prediction=models.CharField(
        verbose_name="Predicted Outcome",
        max_length=1000,
        default="NEUTRAL",
        choices=InterrogationHumanPrediction.choices()
    )
    machine_prediction = JSONField(default=dict,verbose_name="Machine Prediction")
    entities= JSONField(default=dict)
    data = JSONField(default=dict)


    is_active = models.BooleanField(default=True)

    history = HistoricalRecords()
    objects = CustomManagerActiveInterrogator()


    def extend_tendrils(self):
        """
            Generate a chords with a callback that will check if all the tendrils
            are out
        :return:
        """
        sample_size = self.sample_size
        interrogator_id = self.id
        team_id = self.team_id
        user_id = self.user.id

        items = (
            (interrogator_id, user_id, team_id) for item in range(0, sample_size)
        )

        callback = extend_tendril_callback.si(interrogator_id)

        headers = map(
            lambda item: extend_tendril.si(item[0], item[1], item[2]),
            items
        )
        r=chord(headers)(callback)

        logger.info("CHORD COMPLETE EXTENDING INTERROGATOR TENDRILS")

    @property
    def unit_count_completed(self):
        """
        count completed units
        """
        return self.interrogation_units.filter(
            Q(is_complete=True),
        ).count()


    @property
    def completed_percentage(self):
        numerator = self.unit_count_completed
        denominator = self.sample_size
        percentage = round(((float(numerator) / float(denominator)) * 100.00), 0)
        return percentage


    @property
    def margin_of_error(self):
        pop_sqrt = math.sqrt(self.unit_count_completed) or 0.0
        moe = 1.29 / pop_sqrt if pop_sqrt else 0.0
        return round(moe * 100.00, 2)


    @property
    def sentiment_snapshot(self):
        _da_mean = mean([
                    i.compound for i in self.interrogation_units.filter(
                    compound__isnull=False
                    ).order_by(
                        '-date_completed', '-date_registered'
                )])
        _sent = round(float(_da_mean), 3) if _da_mean else 0.0

        return _sent


    def check_for_complete_raise_flag(self):
        """
        this marks this model complete
        if the correct number of interrogation
        units are complete
        :return:
        """
        from interrogator.tasks import inquisitor, inquisitor_scribe_entity_recognition

        __ABORT_THRESHOLD__ = settings.INTERROGATOR_ABORT_THRESHOLD_IN_SECONDS
        _date = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)

        is_count_complete = (self.unit_count_completed >= self.sample_size)
        is_too_old_stop =  ((_date - self.date_registered).total_seconds() > __ABORT_THRESHOLD__)
        logger.warning(f"       ***check_for_complete_raise_flag {is_count_complete} **** {is_too_old_stop}")
        if is_count_complete or is_too_old_stop:
            self.is_complete = True
            self.is_locked = True
            self.date_completed = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)
            self.duration_in_seconds = (self.date_completed - self.date_registered).total_seconds()
            self.retract_tendrils_sentiment = self.sentiment_snapshot
            self.as_typed = self.keyword

            self.save()
            logger.warning(f"       *** endcheck_for_complete_raise_flag {is_count_complete} **** {is_too_old_stop}")
            inquisitor.apply(args=[self.pk])
            inquisitor_scribe_entity_recognition.apply(args=[self.pk])





    def __str__(self):
        return "[ pk:{} ]|:{}:|{}|{}".format(
            self.pk,
            self.user.username,
            self.keyword,
            self.city.name,
        )

    class Meta:
        ordering = ['-pk', F('city__name').desc(), F('user__last_name').asc()]
        verbose_name = "interrogation"
        verbose_name_plural = "interrogations"

admin.site.register(Interrogation)

class InterrogationUnit(models.Model):
    """
    class describes a single interrogation unit
    """
    interrogation = models.ForeignKey('Interrogation', on_delete=models.CASCADE, related_name="interrogation_units")
    # may or may nopt have these
    team = models.ForeignKey('core.Team', null=True, blank=True, on_delete=models.SET_NULL, related_name="teams")
    user = models.ForeignKey('auth.User', null=True, blank=True, on_delete=models.SET_NULL, related_name="user")

    uuid = models.UUIDField(default=uuid.uuid4, db_index=True)
    positive = models.FloatField(null=True, default=0.0)
    negative = models.FloatField(null=True, default=0.0)
    compound = models.FloatField(null=True, default=0.0)
    external_entity_id = models.CharField(max_length=255, null=True, db_index=True)
    is_complete = models.BooleanField(default=False)
    date_registered = models.DateTimeField(blank=True, null=True, auto_now_add=True, db_index=True)
    date_completed = models.DateTimeField(blank=True, null=True, db_index=True)
    duration_in_seconds = models.IntegerField(null=True, blank=0)
    data = JSONField(default=dict)
    is_active = models.BooleanField(default=True)
    history = HistoricalRecords()


    def get_content(self):
        pass

    class Meta:
        ordering = ['-pk', '-date_registered', '-date_completed']
        verbose_name = "interrogation unit"
        verbose_name_plural = "interrogation units"

admin.site.register(InterrogationUnit)