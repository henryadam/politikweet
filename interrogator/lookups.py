# app_name/lookups.py
from ajax_select import register, LookupChannel
from core.models import City
from django.db.models import Q

@register('cities')
class CitiesLookup(LookupChannel):
    model = City
    search_field="name"
    min_length = 3

    def check_auth(self, request):
        """
        only return data for authenticated users as per the docs
        :param request:
        :return:
        """
        return request.user.is_authenticated or None

    def get_result(self, obj):
        result = u"{}, {}".format(obj.name, obj.region.name)
        return result

    def get_query(self, q, request):
        kwargs = {f"{self.search_field}__icontains": q}
        return self.model.objects.filter(**kwargs).order_by(self.search_field)

    def format_item_display(self, item):
        msg = u"<span class='label label-success'>{}, {}</span>".format(item.name, item.region.name)
        return msg

