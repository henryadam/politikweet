import django_tables2 as tables
import itertools

from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.db.models import Q, F
from django.utils.timesince import timesince
from interrogator.models import Interrogation, InterrogationUnit
import datetime
import math
from politikweet.utils import as_percent



class ImageColumn(tables.Column):
    def render(self, value):
        return format_html('<img src="/media/img/{}.jpg" />', value)

class InterrogatorUnitTable(tables.Table):

    date_registered = tables.DateTimeColumn(
        orderable=True,
        verbose_name="Registered",
        localize=True,
        attrs={
            'th':
                {
                    'data-toggle': "tooltip",
                    'data-placement': "top",
                    'title': "date this interrogation unit was created"
                }
        },
        format='m/d/Y H:i:s'
    )

    date_completed = tables.DateTimeColumn(
        orderable=True,
        verbose_name="Completed",
        localize=True,
        format='m/d/Y H:i:s',
        attrs={
            'th':
                {
                    'data-toggle': "tooltip",
                    'data-placement': "top",
                    'title': "date this interrogation unit was completed"
                }
        },
    )

    external_entity_id = tables.Column(
        orderable=True,
        verbose_name="External Entity Id",
        accessor=tables.A("external_entity_id"),
        linkify={
            "viewname": "secure-interrogator-external-entity-id-detail",
            "args": [
                tables.A("uuid"),
                tables.A("external_entity_id"),
            ]},
        attrs={
            'th':
                {
                    'data-toggle': "tooltip",
                    'data-placement': "top",
                    'title': "unique identifier for data at external service, for now twitter id"
                }
        },

    )

    compound = tables.Column(
        orderable=True,
        verbose_name="Sentiment",
        attrs={
            'th':
                {
                    'data-toggle': "tooltip",
                    'data-placement': "auto",
                    'title': "describes the sentiment/opinion polarity;\ncalculated from a basic search at source,\n-1 is the most negative, +1 is the most positive."
                }
        }

    )

    location = tables.Column(
        orderable=True,
        verbose_name="Location",
        attrs={
            'th':
                {
                    'data-toggle': "tooltip",
                    'data-placement': "auto",
                    'title': "the location, in this case, a user defined city\nin the United States or best match within 100mi"
                }
        }
    )

    uuid = tables.Column(
        visible=False,
    )


    # columns = [
    #     'date_completed',
    #     'date_registered',
    #     'external_entity_id',
    #     'compound',
    #     'task'
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


    class Meta:
        template_name = "custom_django_tables.html"
        attrs = {
           "class": "table-responsive",
           "th": {
                "_ordering": {
                    "orderable": "sortable",  # Instead of `orderable`
                    "ascending": "ascend",  # Instead of `asc`
                    "descending": "descend"  # Instead of `desc`
                }
            }
        }


class InterrogatorTable(tables.Table):

    id = tables.Column(
        visible=True,
        accessor=tables.A("id"),
        attrs={
            'th':
                {
                    'data-toggle': "tooltip",
                    'data-placement': "top",
                    'title': "click the link below to navigate to the detail"
                }
        },
        linkify={
            "viewname": "secure-interrogator-detail",
            "args": [
                tables.A("uuid")
            ]},
        verbose_name="Detail"
    )

    date_registered = tables.DateTimeColumn(
        orderable=True,
        attrs={
            'th':
                {
                    'data-toggle': "tooltip",
                    'data-placement': "top",
                    'title': "when this interrogation was created"
                }
        },
        verbose_name="Date Created",
        localize=True,
        format='m/d/Y H:i:s'
    )

    date_completed = tables.DateTimeColumn(
        orderable=True,
        verbose_name = "Date Completed",
        attrs={
            'th':
                {
                    'data-toggle': "tooltip",
                    'data-placement': "top",
                    'title': "when the interrogation finishes"
                }
        },
        localize=True,
        format='m/d/Y H:i:s'
    )

    city = tables.Column(
        attrs={
            'th':
                {
                    'data-toggle': "tooltip",
                    'data-placement': "top",
                    'title': "location...search at city within 100mi"
                }
        },
        orderable=True
    )

    keyword = tables.Column(
        orderable=True,
        verbose_name="Query",
        attrs = {
                'th':
                    {
                        'data-toggle': "tooltip",
                        'data-placement': "top",
                        'title': "the keyword you typed to define the interrogator."
                    }
            },

    )

    sample_size = tables.Column(
        orderable=True,

        visible=False

    )

    completed_count = tables.Column(
        orderable=True,
        attrs={
            'th':
                {
                    'data-toggle': "tooltip",
                    'data-placement': "top",
                    'title': "describes, in percentage and count, progress to completion"
                }
        },
        verbose_name="Progress"
    )

    percentage_complete = tables.Column(
        visible=False,
        orderable=False,
    )

    uuid = tables.Column(
        visible=False,
        orderable=False,
    )

    sentiment_style = tables.Column(
        visible=False,
        orderable=False,

    )
    #
    margin_of_error = tables.Column(
        visible=True,
        orderable=True,
        verbose_name="M.O.E",
        attrs={
            'th':
                {
                    'data-toggle': "tooltip",
                    'data-placement': "auto",
                    'title': "margin of error, given a mean of 0,\n accuracy is described by the sample size"
                }
        }

    )

    sentiment = tables.Column(
        orderable=True,
        verbose_name = "Opinion/Sentiment Analysis",
        attrs={
            'th':
                {
                    'data-toggle': "tooltip",
                    'data-placement': "top",
                    'title': "describes the sentiment/opinion polarity;\ncalculated from a basic search at source,\n -1 is the most negative, +1 is the most positive."
                }
        },

    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_id(self, value, record):
        return mark_safe(format_html(f"""<span class="material-symbols-sharp gi-4x">open_in_new</span>"""))


    def render_sentiment(self,value, record):
        """
        produce a display
        """
        style = record.get('sentiment_style').split(':')
        _h = style[0].split('_').pop()
        trend=None

        if record['sentiment'] < 0:
            trend = "trending down"
        elif record['sentiment'] > 0:
            trend = "trending up"

        try:
            temp = f"""
                       <div class='row'>
                            <div class='col-md-2'>
                                <span class='material-symbols-sharp gi-4x'>{style[0]}</span>
                            </div>
                            <div class='col-md-10'>
                                <p>{_h} {record['sentiment']} - {trend}</p>
                            </div>
                        </div>                        
            """
        except Exception as e:
            temp = f"""
                        <div class='row'>                                                   
                            <div class='col-md-2'>
                                <span class='material-symbols-sharp gi-4x'>{style[0]}</span>
                            </div>        
                            <div class='col-md-10'>
                                <p>{_h} <small>{record['sentiment']} - {trend}</small></p>                                
                            </div>
                        </div>
            """

        return mark_safe(format_html(temp))

    def order_sentiment(self, queryset, is_descending):
        queryset = queryset.annotate(sentiment=F('sentiment')).order_by(("-" if is_descending else "") + "sentiment")
        return (queryset, True)

    def order_completed_count(self, queryset, is_descending):
        queryset = queryset.completed_count(sentiment=F('completed_count')).order_by(("-" if is_descending else "") + "completed_count")
        return (queryset, True)

    def render_completed_count(self, value, record):
        asPercent = record.get('percentage_complete')
        ret = f"{value} of {record['sample_size']} ({int(asPercent)}%)"
        return ret

    def render_percentage_complete(self, value, record):
        asPercent = f'{int(value)}%'
        return asPercent

    def render_margin_of_error(self, value, record):
        asPercent = f'{value}%'
        return asPercent

    def order_percentage_complete(self, queryset, is_descending):
        queryset = queryset.order_by(("-" if is_descending else "") + "percentage_complete")
        return (queryset, True)

    def order_margin_of_error(self, queryset, is_descending):
        queryset = queryset.order_by(("-" if is_descending else "") + "margin_of_error")
        return (queryset, True)

    def render_city(self, value, record):
        _st = value.split(', ')
        _f = f'<small>{_st[0]}, {_st[1]} {_st.pop()}</small>'
        return mark_safe(format_html(_f))

    class Meta:
        template_name = "custom_django_tables.html"
        attrs = {
            "th": {
                "_ordering": {
                    "orderable": "sortable",  # Instead of `orderable`
                    "ascending": "ascend",  # Instead of `asc`
                    "descending": "descend"  # Instead of `desc`
                }
            }
        }





