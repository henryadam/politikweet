from __future__ import absolute_import, unicode_literals, print_function

import datetime
import logging
import math
import tweepy
import pandas
import plotly
import plotly.graph_objects as go
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.cache import caches
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.list import MultipleObjectMixin
from django_tables2.paginators import LazyPaginator
from organizations.views.mixins import OrganizationMixin

from interrogator.forms import InterrogationModelForm
from interrogator.models import Interrogation, InterrogationUnit
from interrogator.tables import InterrogatorTable, InterrogatorUnitTable
from politikweet.mixins import IsAuthenticatedIsInterrogatorAccessMixin, IsAuthenticatedIsLibrarianAccessMixin
from politikweet.utils import human_sentiment, render_human_sentiment, normalize

from django.db.models import Q, F

from allauth.socialaccount.models import SocialAccount, SocialApp, SocialToken

# Get an instance of a logger
logger = logging.getLogger('default')


# and interrogator performs interrogations that are made up of a series or collection of interrogation units.
class CreateInterrogatorView(
    IsAuthenticatedIsInterrogatorAccessMixin,
    IsAuthenticatedIsLibrarianAccessMixin,
    LoginRequiredMixin,
    FormView,
    OrganizationMixin
):
    template_name = "interrogator/interrogation-define.html"
    form_class = InterrogationModelForm
    success_url = reverse_lazy('secure-interrogator-define')


    def dispatch(self, request, *args, **kwargs):
        """
        the dispatch method, a form is rendered to cature
            city
            keyword
            sample size
            and the POST subnitted is used to create
            a model and then we launch the tendrils0000000000000000000000000000000000000000000000000000000
        @param request:
        @param args:
        @param kwargs:
        @return:
        """

        return super(CreateInterrogatorView, self).dispatch(request, *args, **kwargs)


    def get_form_kwargs(self, *args, **kwargs):
        """
        get or set the form kwargs

        @param args:
        @param kwargs:
        @return:
        """
        _kwargs = super().get_form_kwargs(*args, **kwargs)
        _kwargs.update({
            "user": self.request.user
        })
        _kwargs.update(
            {"team_id": self.request.team.id}
        )
        return _kwargs


    def get_context_data(self, **kwargs):
        context = super(CreateInterrogatorView, self).get_context_data(**kwargs)
        return context


    def form_invalid(self, form):
        return super().form_invalid(form)


    def form_valid(self, form):
        interrogator = form.save()
        interrogator.extend_tendrils()
        return super().form_valid(form)


class InterrogatorListView(
    IsAuthenticatedIsInterrogatorAccessMixin,
    IsAuthenticatedIsLibrarianAccessMixin,
    LoginRequiredMixin,
    TemplateView,
    MultipleObjectMixin,
    OrganizationMixin
):
    """
    Interrogator dashboard here you can see a list of your interrogators
    """
    template_name = "interrogator/dashboard.html"
    allow_empty = True
    permission_denied_message = "You are not Interrogator or you lack the permissions to access Interrogator data"
    paginate_by = 10
    paginator_class = LazyPaginator
    model = Interrogation
    context_object_name = "interrogations"
    queryset = Interrogation.objects.all()
    object_list = queryset
    paginator = None


    def dispatch(self, request, *args, **kwargs):
        return super(InterrogatorListView, self).dispatch(request, *args, **kwargs)


    def get_context_data(self, **kwargs):
        context = super(InterrogatorListView, self).get_context_data(**kwargs)
        interrogatory_list = []
        interrogations = self.queryset.filter(user=self.request.user).order_by('-date_completed', '-date_registered')
        plot_cache = caches['plots']

        for interrogation in interrogations:
            _payload = {}
            _sent = interrogation.sentiment_snapshot
            _payload['id'] = interrogation.pk
            _payload['keyword'] = interrogation.keyword
            _payload['percentage_complete'] = interrogation.completed_percentage
            _payload['sample_size'] = interrogation.sample_size
            _payload['completed_count'] = interrogation.unit_count_completed
            _payload['city'] = interrogation.city.display_name
            _payload['date_registered'] = interrogation.date_registered or None
            _payload['date_completed'] = interrogation.date_completed or None

            if interrogation.date_completed:
                delta = interrogation.date_completed - interrogation.date_registered
                _payload['duration_of'] = delta.total_seconds() * 1000
                interrogation.duration_of = _payload['duration_of']
                interrogation.save()
            else:
                _payload['duration_of'] = 0

            _payload['sentiment'] = _sent
            _payload['sentiment_style'] = human_sentiment(_sent)
            _payload['population'] = interrogation.city.population

            # The margin of error is the radius(half) of# the 99 % confidence
            # interval, or 2.58 # standard# errors, when p = 50 %.As such, it
            # can be alculated  directly from the number of poll respondents.
            # To conclude, the margin of error is the
            # 99 % confidence  interval for a reported percentage of 50 %.If p moves away from 50 %,
            # the confidence interval around p will be smaller.
            # Thus, the margin of error represents an upper bound to the uncertainty;
            # one is at least 99 % certain that the "true" percentage is within a margin of
            # error of a reported percentage for any reported percentage.

            _payload['margin_of_error'] = interrogation.margin_of_error

            _payload['uuid'] = f"{interrogation.uuid}"

            interrogatory_list.append(_payload)

        context[self.context_object_name] = interrogatory_list

        tt = InterrogatorTable(
            data=interrogatory_list,
            page_field="page",
            order_by_field="sort",
            order_by=['-keyword','-completed_count', 'sentiment','-date_completed', '-date_registered'],
            orderable=True,
            request=self.request
        )
        tt.paginate(page=self.request.GET.get("page", 1), per_page=25)
        context['table'] = tt
        return context


class InterrogatorDetailView(
    IsAuthenticatedIsInterrogatorAccessMixin,
    IsAuthenticatedIsLibrarianAccessMixin,
    LoginRequiredMixin,
    TemplateView,
    SingleObjectMixin,
    OrganizationMixin
):
    template_name = "interrogator/interrogation-detail.html"
    model = Interrogation
    context_object_name = "interrogation"
    object = None
    queryset = Interrogation.objects.all()


    def dispatch(self, request, *args, **kwargs):
        return super(InterrogatorDetailView, self).dispatch(request, *args, **kwargs)


    def heatmap(self, interrogators, *args, **kwargs):
        fig_as_html = None

        interrogation_units = InterrogationUnit.objects.filter(
            interrogation__in=interrogators,
            external_entity_id__isnull=False,
        ).annotate(
            duration=F('date_registered') - F('date_completed')
        ).annotate(
            duration_millis=F('duration_in_seconds') * 1000 if F('duration_in_seconds') else None
        ).values_list(
            'date_completed', 'date_registered', 'external_entity_id', 'compound', 'duration_millis'
        ).order_by(
            "-date_completed",
            "-external_entity_id"
        )

        df = pandas.DataFrame(data=interrogation_units, columns=[
            'date_completed',
            'date_registered',
            'external_entity_id',
            'compound',
            'duration_millis'
        ])

        layout = go.Layout(
            clickmode="event+select",
            hovermode="closest",
            plot_bgcolor='#070d22',
            paper_bgcolor='#070d18',
            autosize=True,
            xaxis=dict(
                showticklabels=True,
                tickformat="%m-%d-%Y %H:%M:%S",
                type="date",
                title=dict(
                    text="""c r e a t e d""",
                    font=dict(
                        size=11,
                    ),
                ),
                color="#ffffff"
            ),
            yaxis=dict(

                showticklabels=True,
                tickcolor="#ffffff",
                tickformat="%m-%d-%Y %H:%M:%S",
                type="date",
                title=dict(
                    text="""p r o c e s s e d""",
                    font=dict(
                        size=11
                    ),
                ),
                color="#ffffff",

            ),
            font=dict(family="Ubuntu", color="#ffffff"),
            title=dict(
                text='SENTIMENT as COLOR : acquisition (Y) DATE vs. creation (X) DATE',
                font=dict(
                    size=12,

                )
            )
        )

        fig = go.Figure(data=go.Scatter(
            x=df['date_registered'],
            y=df['date_completed'],
            text=df['external_entity_id'],
            mode='markers',
            marker=dict(
                size=10,
                color=df['compound'],
                colorscale=['red', 'orange', 'green'],
                showscale=True,
            )
        ),
            layout=layout,
        )
        fig.update_layout(coloraxis_colorbar=dict(
                title="Sentiment Polarity",
                showticklabels=True,
                ticktext=['(-1) most negative', '(0) nuetral', '(1) most positive'],
                tickvals=[-1, 0, 1],
                ticks="outside",
                ticklabelposition='outside bottom',

                xpad=10,
                ypad=10,

            ))

        fig_as_html = plotly.io.to_html(
            fig,
            full_html=False,
            config={'displayModeBar': True}
        )

        return fig_as_html


    def plot_table(self, poll=None, units=None, *args, **kwargs):
        """

        """
        units = units.filter(external_entity_id__isnull=False).only(
            'uuid',
            'date_completed',
            'date_registered',
            'external_entity_id',
            'compound',
            'data'
        ).order_by(
            '-date_completed',
            '-date_registered',
            '-external_entity_id',
        )
        for_table = list()
        for unit in units.iterator():
            payload = dict()
            payload.update({'date_completed': unit.date_completed})
            payload.update({'date_registered': unit.date_registered})
            payload.update({'compound': unit.compound})
            payload.update({'external_entity_id': unit.external_entity_id})
            payload.update({'task': unit.data.get('task')})
            payload.update({'location': unit.data.get('location')})
            payload.update({'uuid': unit.uuid})
            for_table.append(payload)

        return for_table


    def get_context_data(self, **kwargs):
        context = super(InterrogatorDetailView, self).get_context_data(**kwargs)
        self.object = self.queryset.get(uuid=kwargs.get('uuid'))
        units = self.object.interrogation_units.all()
        _sent = human_sentiment(self.object.sentiment_snapshot)
        sentiment_style = _sent.split(':')[0]
        sentiment_trend = _sent.split(':')[1]
        sentiment_style_name = f"{sentiment_style.split('_')[0]} {sentiment_style.split('_')[1]}"
        context.update({'moe': self.object.margin_of_error})
        context.update({'sentiment': self.object.sentiment_snapshot})
        context.update({'sentiment_style': sentiment_style})
        context.update({'sentiment_trend': sentiment_trend})
        context.update({'sentiment_style_name': sentiment_style_name})
        context.update({'units': units})
        context.update({"interrogator": self.object})

        # {
        #     '_state': < django.db.models.base.ModelState
        # object
        # at
        # 0x7f1c4ece96d0 >,
        # 'id': 20,
        # 'sample_size': 500,
        # 'user_id': 5,
        # 'team_id': 2,
        # 'is_locked': False,
        # 'is_complete': False,
        # 'must_include_url': False,
        # 'keyword': '#adidas',
        # 'as_typed': None,
        # 'city_id': 833,
        # 'uuid': UUID('adde982a-2db2-46a0-baae-bf157a0d942c'),
        # 'date_registered': datetime.datetime(2022, 12, 19, 4, 4, 24, 612329, tzinfo= < UTC >),
        # 'date_completed': None,
        # 'duration_in_seconds': None,
        # 'retract_tendrils_sentiment': 0.0,
        # 'human_prediction': 'VERYNEGATIVE',
        # 'machine_prediction': {},
        # 'entities': {},
        # 'data': {},
        # 'is_active': True
        # }
        context.update({"poll_unit_heatmap": self.heatmap([self.object])})  # poll_unit_table
        context.update({"pollunit_table": self.plot_table(
            poll=self.object,
            units=units
        )})

        tt = InterrogatorUnitTable(
            data=context.get('pollunit_table'),
            page_field="page",
            order_by_field="sort",
            order_by=['keyword', '-completed_count', '-date_completed', '-date_registered', ],
            orderable=True,
            request=self.request
        )

        tt.paginate(page=self.request.GET.get("page", 1), per_page=10)

        context.update({'table_tt': tt})

        return context


class InterrogationUnitExternalEntityIdView(
    IsAuthenticatedIsInterrogatorAccessMixin,
    IsAuthenticatedIsLibrarianAccessMixin,
    LoginRequiredMixin,
    TemplateView,
    OrganizationMixin
):
    template_name = "interrogator/external-entity-id.html"
    allow_empty = True
    permission_denied_message = "You are not Interrogator or you lack the permissions to access Interrogation data"
    model = InterrogationUnit
    object = None
    queryset = InterrogationUnit.objects.all()


    def dispatch(self, request, *args, **kwargs):
        # TODO: change this to accept the uuid for the interrogation unit
        self.object = self.queryset.get(
            external_entity_id=kwargs.get('external_entity_id'),
            uuid = kwargs.get('uuid')
        )
        return super(InterrogationUnitExternalEntityIdView, self).dispatch(request, *args, **kwargs)


    def get_context_data(self, **kwargs):
        context = super(InterrogationUnitExternalEntityIdView, self).get_context_data(**kwargs)


        account = SocialAccount.objects.select_related('user').get(
            user_id=self.request.user,
            provider__iexact='twitter',
        )

        app = SocialApp.objects.filter(
            provider__iexact='twitter',
            key__isnull=False,
            client_id__isnull=False,
            secret__isnull=False).order_by('-pk').first() # this will give us the latest entry

        tokens = {
            "access_token": account.socialtoken_set.all().first().token,
            "access_token_secret": account.socialtoken_set.all().first().token_secret
        }

        auth = tweepy.OAuth1UserHandler(
            app.client_id,
            app.secret,
            **tokens
        )

        context['status'] = tweepy.API(
            auth=auth, wait_on_rate_limit=True
        ).get_status(self.object.external_entity_id, tweet_mode="extended") if auth else {}

        context['sentiment'] = render_human_sentiment(
            {
                "sentiment_style":human_sentiment(self.object.compound),
                "sentiment":self.object.compound
            }
        )
        context['keyword'] = self.object.interrogation.keyword
        context['named_entities'] = self.object.data.get('named_entities',[]) or None
        context['urls'] = [ item.get('expanded_url', None) for item in context['status'].entities['urls'] ] or None
        context['hashtags'] = [ "#" + item.get('text') for item in context['status'].entities['hashtags'] ] or None
        return context
