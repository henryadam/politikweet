from ajax_select.fields import AutoCompleteSelectField
from crispy_forms.helper import FormHelper
from crispy_forms.layout import ButtonHolder, Layout, Submit, Div, Column, Field, HTML
from django.forms import ModelForm
from django.forms.widgets import HiddenInput, RadioSelect

from core.models import Team
from interrogator.models import Interrogation
from politikweet.exceptions import CannotDeriveTeam


# create a ModelForm
class InterrogationModelForm(ModelForm):
    def __init__(self, *args, **kwargs):
        """
        describes an interrogation object, an interrogation is a basic key word that
        will be used against the twitter search describes one poll

        @param args:
        @param kwargs:
        """
        user = kwargs.pop('user')
        team_id = kwargs.pop('team_id')

        super(InterrogationModelForm, self).__init__(*args, **kwargs)

        team = Team.objects.filter(pk=team_id).first()

        self.fields['user'].initial = user

        if team:
            self.fields['team'].initial = team
        else:
            raise CannotDeriveTeam

        self.fields['city'] = AutoCompleteSelectField('cities', required=True, plugin_options={
                'delay': 444,
                'position': {
                    'my': "right top",
                    'at': "right bottom"
                }
            }  )
        self.fields['city'].help_text = """The United States city you wish to target.
         This is a best attempt, we take what twitter gives. We must know where to look,
        too much geography and the samples are meaningless."""

        self.helper = FormHelper(self)
        self.helper.field_class="form-check-inline"
        self.helper.help_text_inline = True
        self.helper.layout = Layout(
                Div(
                    Column(
                        Field('keyword'),
                        Field('city'),
                        css_class="col-lg-6"),
                    Column(
                        Field('human_prediction'),
                        Field('user'),
                        Field('team')
                        ,css_class="col-lg-offset-1 col-lg-5"),
                    css_class="row"
                ),
        )
        # You can dynamically adjust your layout
        self.helper.layout.append(
            ButtonHolder(
                Submit(
                    'interrogation-dashboard',
                    'Create Interrogation',
                    css_class="btn-lg"
                ),

            ))

    class Meta:
        model = Interrogation
        fields = ["keyword", "city","human_prediction", "user", "team"]
        exclude = ["entities","sample_size", "is_complete", "is_locked", "must_include_url","as_typed" ]

        help_texts = {
            "sample_size": "How many interrogation units you wish to analyze, the higher the value the less margin of error. As the sample size grows so does time to completion, accuracy is expensive and may not be necessary try the defaults first.",
            "keyword": "a keyword, (#) hashtag or (@) mention your target search, the simpler the better.",
            "human_prediction":"Make a prediction and discover how it stands up against analysis, the idea is to reinforce your intuition."

        }
        widgets = {
            'user': HiddenInput(),
            'team': HiddenInput(),
            'human_prediction': RadioSelect(),
        }

