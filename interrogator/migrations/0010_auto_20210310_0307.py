# Generated by Django 2.2.18 on 2021-03-10 03:07

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('interrogator', '0009_auto_20210309_0320'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalinterrogation',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4),
        ),
        migrations.AlterField(
            model_name='interrogation',
            name='team',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='interrogations', to='core.Team'),
        ),
        migrations.AlterField(
            model_name='interrogation',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='interrogations', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='interrogation',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4),
        ),
        migrations.AlterField(
            model_name='interrogationunit',
            name='team',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='teams', to='core.Team'),
        ),
        migrations.AlterField(
            model_name='interrogationunit',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='user', to=settings.AUTH_USER_MODEL),
        ),
    ]
