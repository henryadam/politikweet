# Generated by Django 2.2.18 on 2022-12-06 03:27

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('interrogator', '0022_auto_20221205_0252'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalinterrogation',
            name='human_prediction',
            field=models.CharField(max_length=1000, verbose_name='Predicted Outcome'),
        ),
        migrations.AlterField(
            model_name='historicalinterrogation',
            name='keyword',
            field=models.CharField(max_length=1000, verbose_name='Keyword Phrase'),
        ),
        migrations.AlterField(
            model_name='historicalinterrogation',
            name='machine_prediction',
            field=django.contrib.postgres.fields.jsonb.JSONField(default=dict, verbose_name='Machine Prediction'),
        ),
        migrations.AlterField(
            model_name='historicalinterrogation',
            name='sample_size',
            field=models.IntegerField(default=500, verbose_name='Poll Size'),
        ),
        migrations.AlterField(
            model_name='interrogation',
            name='human_prediction',
            field=models.CharField(max_length=1000, verbose_name='Predicted Outcome'),
        ),
        migrations.AlterField(
            model_name='interrogation',
            name='keyword',
            field=models.CharField(max_length=1000, verbose_name='Keyword Phrase'),
        ),
        migrations.AlterField(
            model_name='interrogation',
            name='machine_prediction',
            field=django.contrib.postgres.fields.jsonb.JSONField(default=dict, verbose_name='Machine Prediction'),
        ),
        migrations.AlterField(
            model_name='interrogation',
            name='sample_size',
            field=models.IntegerField(default=500, verbose_name='Poll Size'),
        ),
    ]
