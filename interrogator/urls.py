"""politikweet URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/{{ docs_version }}/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from __future__ import absolute_import, unicode_literals, print_function

from ajax_select import urls as ajax_select_urls
from django.conf.urls import include
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import path

from interrogator.views import InterrogatorDetailView, InterrogatorListView, CreateInterrogatorView, \
    InterrogationUnitExternalEntityIdView

admin.autodiscover()


urlpatterns = [
    path('dashboard/', login_required(InterrogatorListView.as_view()), name="secure-interrogator-dashboard"),
    path('define/', login_required(CreateInterrogatorView.as_view()), name="secure-interrogator-define"),
    path('detail/<str:uuid>/', login_required(InterrogatorDetailView.as_view()), name="secure-interrogator-detail"),
    path('external-entity-id/detail/<str:uuid>/<str:external_entity_id>/', login_required(InterrogationUnitExternalEntityIdView.as_view()), name="secure-interrogator-external-entity-id-detail"),
    path('async-select/', include(ajax_select_urls)),
]
