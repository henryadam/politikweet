from __future__ import absolute_import, unicode_literals, print_function

import datetime
import logging
import uuid

import pytz
from celery import chord
from django.conf import settings

from mongoengine import DynamicDocument
from mongoengine import fields

from interrogator.enums import InterrogationUnitSampleSize
from interrogator.tasks import extend_tendril, extend_tendril_callback

# Get an instance of a logger
logger = logging.getLogger('console')


class Interrogation(DynamicDocument):
    """
      describes a single unit of search work
    """
    sample_size = fields.IntField(
        default=settings.DEFAULT_INTERROGATION_UNIT_SAMPLE_COUNT,
        choices=InterrogationUnitSampleSize.choices()
    )
    user = fields.IntField(
        null=True,

    )

    team = fields.IntField(
        null=True,

    )
    is_locked = fields.BooleanField(default=False)
    is_complete = fields.BooleanField(default=False)
    must_include_url = fields.BooleanField(default=False)
    keyword = fields.StringField(max_length=255)
    city = fields.IntField()
    uuid = fields.UUIDField(default=uuid.uuid4, )
    date_registered = fields.DateTimeField(null=True, auto_now_add=True, )
    date_completed = fields.DateTimeField(null=True, )
    retract_tendrils_sentiment = fields.FloatField(null=True, default=0.0, )
    data = fields.DictField(default=dict)

    def extend_tendrils(self, user):
        """
            Generate a chords with a callback that will check if all the tendrils
            are out
        :return:
        """

        sample_size = self.sample_size
        interrogator_id = self.id
        team_id = self.team_id
        user_id = user.id

        items = (
            (interrogator_id, user_id, team_id) for item in range(0, sample_size)
        )
        callback = extend_tendril_callback.si(interrogator_id)

        headers = map(
            lambda item: extend_tendril.si(item[0], item[1], item[2]),
            items
        )
        chord(headers)(callback)

        logger.info("MONGO PEXTENDING INTERROGATOR TENDRILS {}".format(repr(self)))

    @cached_property
    def completed_interrogation_unit_count(self):
        return self.interrogation_units.filter(
            Q(is_complete=True),
        ).count()

    @cached_property
    def completed_percentage(self):
        numerator = self.completed_interrogation_unit_count
        denominator = self.sample_size
        percentage = round(((float(numerator) / float(denominator)) * 100.00), 0)
        return percentage

    def if_completed_raise_flag(self):
        """
        this marks this model complete if the correct number of interrogation
        units are complete
        :return:
        """
        from interrogator.inquisitors.tasks import inquisitor
        if self.completed_interrogation_unit_count >= self.sample_size:
            self.is_complete = True
            self.is_locked = False
            self.date_completed = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)
            self.save()
            inquisitor.apply(interrogator_id=self.pk)

    def __str__(self):
        return "[ pk:{} ]|:{}:|{}|{}".format(
            self.pk,
            self.user.username,
            self.keyword,
            self.city.name,
        )

    class Meta:
        ordering = ['-pk', F('city__name').desc(), F('user__last_name').asc()]
        verbose_name = "interrogation"
        verbose_name_plural = "interrogations"


class InterrogationUnit(DynamicDocument):
    """
    class describes a single interrogation unit
    """
    interrogation = fields.IntField(null=True, )
    # may or may nopt have these
    team = fields.IntField(null=True, blank=True)
    user = fields.IntField(null=True, )

    uuid = fields.UUIDField(default=uuid.uuid4, )
    positive = fields.FloatField(null=True, default=0.0)
    negative = fields.FloatField(null=True, default=0.0)
    compound = fields.FloatField(null=True, default=0.0)
    external_entity_id = fields.CharField(max_length=255, null=True, )
    is_complete = fields.BooleanField(default=False)
    date_registered = fields.DateTimeField(null=True, auto_now_add=True, )
    date_completed = fields.DateTimeField(null=True, )
    data = fields.DictField(default=dict)

    class Meta:
        ordering = ['-pk', '-date_registered', '-date_completed']
        verbose_name = "interrogation unit"
        verbose_name_plural = "interrogation units"
