from enum import IntEnum, Enum
from django.conf import settings


class InterrogationUnitSampleSize(IntEnum):   # A subclass of Enum
    SMALL = 500
    MEDIUM = 1000
    LARGE = 3000
    XLARGE = 5000

    @classmethod
    def choices(cls):
        defn = ('','--- please make a selection ---')

        return [defn] + [(key.value, "{}({})".format(key.name, key.value)) for key in cls]

class InterrogationHumanPrediction(Enum):
    REALLYPOSITIVE = "REALLY POSITIVE" # -1
    VERYPOSITIVE= "VERY POSITIVE"
    POSITIVE = "POSITIVE"
    NEUTRAL = "AMBIVALENT (NEUTRAL)" # 0
    NEGATIVE = "NEGATIVE"
    VERYNEGATIVE = "VERY NEGATIVE"
    REALLYNEGATIVE = "REALLY NEGATIVE" # -1
    REALLYSUPERNEGATIVE = "REALLY SUPER NEGATIVE"

    @classmethod
    def choices(cls):
        cchh=[ (key.name, "{}".format(key.value)) for key in cls ]

        return cchh





