from __future__ import absolute_import
from celery.utils.log import get_task_logger
from politikweet.celery import app
from politikweet.tasks import PolitikweetBaseTask

logger = get_task_logger(__name__)


@app.task(bind=True, queue="tendrilsQ", routing_key="interrogator.tasks.tendrilsCB", priority=5,
          base=PolitikweetBaseTask)
def extend_tendril_callback(self, interrogation_id):
    """
    at the end of the tendril chord, check to see if were finished
    :param self:
    :param interrogation_id:
    :return:
    """
    from interrogator.models import Interrogation
    interrogation = Interrogation.objects.get(pk=interrogation_id)
    interrogation.check_for_complete_raise_flag()
    return True


@app.task(bind=True, queue="tendrilsQ", routing_key="interrogator.tasks.tendrils", ingnore_results=True, priority=5,
          base=PolitikweetBaseTask)
def extend_tendril(self, interrogation_id, user_id, team_id):
    """
     this is a task that creates an interrogation unit, an interrogation unit
     is a tweet that will be analyzed
    :param self:
    :param interrogation_id:
    :param user_id:
    :param team_id:
    :return:
    """
    from interrogator.models import Interrogation, InterrogationUnit
    from core.models import Team
    from django.contrib.auth.models import User

    # logger.warning('>>> TENDRIL EXTENDS {}'.format(repr(interrogation_id)))
    interrogation = Interrogation.objects.get(pk=interrogation_id)
    user = User.objects.get(pk=user_id)
    team = Team.objects.get(pk=team_id)
    task_id = self.request.id

    created_interrogation_unit = InterrogationUnit.objects.create(
        interrogation=interrogation,
        is_active=True,
        user=user,
        team=team,
        data={
            "task": task_id
        }
    )



@app.task(
    bind=True,
    queue="interrogatorQ",
    routing_key='interrogator.tasks.interrogate',
    ignore_result=True,
    priority=10,
    rate_limit="5/m",  # we have to wait fort peeps to tweetits 150 per user per 15 minutes at concurrency 4 one worker to be safe it's 3 per minute
    base=PolitikweetBaseTask)
def interrogate(self):
    """
    finds a random interrogation unit that has not been completed
    been analyzed and runs the dang thing
    :param self:
    :return:
    """
    from interrogator.models import InterrogationUnit
    from politikweet.lib.sentimentacron import sentimentalyzer
    from allauth.socialaccount.models import SocialAccount, SocialApp, SocialToken
    from politikweet.utils import random_wobble
    from django.core.cache import caches
    import tweepy
    import random
    import nltk
    import datetime
    import pytz
    import time

    def extract_entity_names(t):
        entity_names = []
        if hasattr(t, 'label') and t.label:
            if t.label() == 'NE':
                entity_names.append(' '.join([child[0] for child in t]))
            else:
                for child in t:
                    entity_names.extend(extract_entity_names(child))
        return entity_names

    retval = False
    _eta = None
    key = None
    weekinseconds = ((60 * 60) * 24) * 7
    interrogation_unit = None

    try:
        saved = 0
        rejected=0
        interrogatories=[]

        query=None

        __PROVIDER__ = 'twitter'
        _filtered_ = []
        politikweet_cache = caches['interrogations']
        politikweet_tweet_cache = caches['tweets']

        units_pks = list(
            # pick the oldest first
            InterrogationUnit.objects.filter(is_complete=False).values_list('pk', flat=True).order_by('-id')[:5000]
        )

        unit_pk = None

        # check to see if we need to get out
        if not units_pks:
            logger.warning("***** NO MORE INCOMPLETE INTERROGATION UNITS, GET OUT!")
            return None
        else:
            unit_pk = random.sample(units_pks, 10) if len(units_pks) >= 10 else random.sample(units_pks, len(units_pks))

        try:
            random_pk = random.choice(units_pks)
        except Exception as e:
            raise e

        interrogation_unit = InterrogationUnit.objects.select_related(
            'interrogation',
            'user'
        ).get(
            is_complete=False,
            pk=random_pk
        )

        key = ":interrogation-unit:{}".format(interrogation_unit.uuid)

        sequestered = None

        #logger.warning({'sequestered':sequestered})

        if sequestered is not None:
            #logger.warning("**** unit sequestered IN CACHE {}".format(key))
            raise Exception('Sequestered try again...')



        if interrogation_unit:
            logger.warning("***** interrogation unit available ----> continue {}".format(repr(interrogation_unit)))
            user = interrogation_unit.interrogation.user
            team = interrogation_unit.interrogation.team
            city = interrogation_unit.interrogation.city.display_name

            lat, long = (
                interrogation_unit.interrogation.city.latitude,
                interrogation_unit.interrogation.city.longitude
            )
            keyword = interrogation_unit.interrogation.keyword
            account = SocialAccount.objects.select_related('user').get(
                user_id=user.id,
                provider__iexact='twitter',
            )
            app = SocialApp.objects.filter(
                provider__iexact='twitter',
                key__isnull=False,
                client_id__isnull=False,
                secret__isnull=False).order_by('-pk').first()  # this will give us the latest entry

            app.refresh_from_db()
            account.refresh_from_db()
            tokens={
                "access_token":account.socialtoken_set.all().first().token,
                "access_token_secret":account.socialtoken_set.all().first().token_secret
            }
            auth = tweepy.OAuth1UserHandler(
                app.client_id,
                app.secret,
                **tokens
            )

            if auth:
                API = tweepy.API(auth=auth,wait_on_rate_limit=True)

                #logger.warning({'API':API or None})

                max_twitter_id = getattr(
                    InterrogationUnit.objects.filter(
                        interrogation_id=interrogation_unit.interrogation_id,
                        external_entity_id__isnull=False
                    ).order_by(
                        '-external_entity_id'
                    ).first(),'external_entity_id', None
                )

                query = '{keyword}'.format(keyword=keyword,)
                geocode="{lat},{long},100mi".format(long=long, lat=lat)
                # logger.warning({'query':query})
                # logger.warning({'geocode': geocode})

                interrogatories = API.search_tweets(
                    q=query,
                    geocode=geocode,
                    count=100,
                    lang='en',
                    since_id=max_twitter_id if max_twitter_id else None
                )

                sentimentalize = sentimentalyzer()

                #TODO: maybe we filter
                filtered_interrogatories = interrogatories

                for idx, interrogatory in enumerate(list(filtered_interrogatories)):

                    if idx == 0:
                        unit = interrogation_unit
                    else:
                        # get a new unit at random from the smae set
                        unit=InterrogationUnit.objects.filter(
                            is_complete=False,
                            external_entity_id__isnull=True,
                            interrogation_id=interrogation_unit.interrogation.id,
                        ).order_by('?').first()

                    logger.warning(unit)

                    status = interrogatory

                    # this check if the tweet text exist for this interrogation
                    if unit and status and not InterrogationUnit.objects.filter(
                            interrogation_id=unit.interrogation.id,
                            external_entity_id=status.id
                    ).exists():

                        # should the tweet be truncated, go get the whole tweet
                        if status.truncated:
                            status = API.get_status(id=status.id)

                        tweet_text = status.text
                        tweet_id = status.id

                        words_tokens = nltk.word_tokenize(tweet_text)
                        token_tokens = nltk.pos_tag(words_tokens)
                        sentences = nltk.sent_tokenize(tweet_text)

                        tokenized_sentences = [nltk.word_tokenize(sentence) for sentence in sentences]
                        tagged_sentences = [nltk.pos_tag(sentence) for sentence in tokenized_sentences]
                        chunked_sentences = nltk.ne_chunk_sents(tagged_sentences, binary=True)

                        entities = []
                        extractions = []

                        for tree in chunked_sentences:
                            entities.extend(extract_entity_names(tree))

                        for entity in entities:
                            if entity not in "RT":
                                extractions.append(entity.title())


                        sentiment_score = sentimentalize.sentiment_analysis(tweet_text)

                        unit.compound = sentiment_score["compound"]
                        unit.positive = sentiment_score["positive_score"]
                        unit.negative = sentiment_score["negative_score"]

                        unit.data.update({
                            "location": getattr(status.user, 'location',None) or getattr(status,'place',None).name or getattr(
                                status,'geo', None
                            ) or getattr(status, 'coordinates' ,None)
                        })

                        unit.data.update({'named_entities':extractions})

                        unit.external_entity_id = tweet_id
                        # add it to  the cache

                        unit.is_complete = True
                        unit.date_completed = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)


                        if not InterrogationUnit.objects.filter(
                                interrogation_id=unit.interrogation.id,
                                external_entity_id=tweet_id
                        ).exists():
                            unit.save()
                            saved=saved+1
                            unit.refresh_from_db()
                            retval = True

                        else:
                            rejected = rejected + 1
                        unit.interrogation.check_for_complete_raise_flag()
                        continue
                    else:
                        rejected = rejected + 1
                        continue
            else:
                #logger.warning("3 --- CANNOT ACQUIRE TWITTER TOKEN")
                raise Exception("3 --- CANNOT ACQUIRE TWITTER TOKEN")
        else:
            #logger.warning("4 --- CANNOT ACQUIRE INTERROGATION UNIT cant find or sequestered")
            raise Exception("4 --- CANNOT ACQUIRE INTERROGATION UNIT : cant find or is sequestered")
    except Exception as e:
        import traceback
        logger.warning(repr(traceback.print_exc()))

    finally:
        city=None

        logger.warning("[ from search: {} filtered: {} ][ rejected: {} ][accepted: {} ][ query:{} ] {}".format(
            len(interrogatories),
            _filtered_,
            rejected,
            saved,
            query,
            city or "cant derive city"))

        return bool(saved)


@app.task(
    bind=True,
    queue="inquisitorQ",
    routing_key="interrogator.tasks.inquisitor",
    ignore_result=True,
    priority=7,
    base=PolitikweetBaseTask, )
def inquisitor(self, interrogation_id, *args, **kwargs):
    """
    find a random interrogation or use the one
    provided that is complete and do the average analysis
    :return:
    """
    import random
    from interrogator.models import Interrogation
    from interrogator.tasks import inquisitor_scribe_entity_recognition
    retval = None
    interrogation = None
    inter_id = interrogation_id or kwargs.get('interrogation_id')
    interrogation = [ inter_id ]

    if interrogation and inter_id:
        _interrogation_id = random.choice(interrogation) if len(interrogation) else []

        logger.warning("  _interrogation_id ****** {}".format(_interrogation_id))

        _interrogation = Interrogation.objects.get(pk=_interrogation_id)

        interrogation_units = _interrogation.interrogation_units.filter(
            is_complete=True
        ).only(
            'positive',
            'negative',
            'compound',
        )

        completed_interrogation_unit_count=len(interrogation_units)

        logger.warning("****** completed_interrogation_unit_count {}".format(completed_interrogation_unit_count))

        if interrogation_units:
            avg_pos = sum(item.positive for item in interrogation_units) / completed_interrogation_unit_count
            avg_neg = sum(item.negative for item in interrogation_units) / completed_interrogation_unit_count
            avg_comp = sum(item.compound for item in interrogation_units) / completed_interrogation_unit_count
            _interrogation.data.update(
                {
                    "positive": avg_pos,
                    "negative": avg_neg,
                    "compound": avg_comp
                }
            )
            _interrogation.retract_tendrils_sentiment = avg_comp
            _interrogation.save()

            logger.warning("{} : INQUISITOR SENTIMENT CALCUALTED".format(repr(_interrogation)))
            retval = True

    logger.info(":::TASK:INQUSISTOR-FINALLY:::: {} ".format(retval))
    return retval


@app.task(
    bind=True,
    queue="inquisitorQ",
    routing_key="interrogator.tasks.inquisitor",
    ignore_result=True,
    priority=5,
    base=PolitikweetBaseTask, )
def inquisitor_scribe_entity_recognition(self, interrogation_id, *args, **kwargs):
    """
     work out the bnamed entities
    """
    import random
    from interrogator.models import Interrogation, InterrogationUnit
    from nltk import FreqDist

    inter_id = interrogation_id or kwargs.get('interrogation_id')
    logger.warning(f"{inter_id} : inter_id")
    interrogation = Interrogation.objects.filter(pk=inter_id).order_by('-pk').first()
    units = interrogation.interrogation_units.only('data').all()
    items=[]
    for unit in units:
        for ent in unit.data.get('named_entities',[]):
            items.append(ent.strip())
    most_common= FreqDist(items).most_common(7)
    # [('Gatorland', 4), ('Get', 3), ('Centralfloridatoyota', 3)]
    interrogation.entities={'common':most_common}
    interrogation.save()


