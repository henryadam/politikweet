from django.contrib.auth.models import Group, Permission, User
from django.test import TransactionTestCase, RequestFactory, Client
from django.test import override_settings
from django.urls import reverse
from model_mommy import mommy
import unittest
from unittest.mock import patch

import tweepy

from core.models import Team, City
from interrogator.models import InterrogationUnit, Interrogation
from politikweet.tests import PolitikweetBaseTestCase
from tweepy.models import Status
from tweepy.api import API
import json

DATA = {
	"created_at": "Wed Oct 10 20:19:24 +0000 2018",
	"id": 1050118621198921728,
	"id_str": "1050118621198921728",
	"text": "To make room for more expression, we will now count all emojis as equal—including those with gender‍‍‍ and skin t… https://t.co/MkGjXf9aXm",
	"truncated": True,
	"entities": {
		"hashtags": [],
		"symbols": [],
		"user_mentions": [],
		"urls": [{
			"url": "https://t.co/MkGjXf9aXm",
			"expanded_url": "https://twitter.com/i/web/status/1050118621198921728",
			"display_url": "twitter.com/i/web/status/1…",
			"indices": [
				117,
				140
			]
		}]
	},
	"source": "<a href='http: //twitter.com'>Twitter Web Client</a>",
	"in_reply_to_status_id": None,
	"in_reply_to_status_id_str": None,
	"in_reply_to_user_id": None,
	"in_reply_to_user_id_str": None,
	"in_reply_to_screen_name": None,
	"user": {
		"id": 6253282,
		"id_str": "6253282",
		"name": "Twitter API",
		"screen_name": "TwitterAPI",
		"location": "San Francisco, CA",
		"description": "The Real Twitter API. Tweets about API changes, service issues and our Developer Platform. Don't get an answer? It's on my website.",
		"url": "https://t.co/8IkCzCDr19",
		"entities": {
			"url": {
				"urls": [{
					"url": "https://t.co/8IkCzCDr19",
					"expanded_url": "https://developer.twitter.com",
					"display_url": "developer.twitter.com",
					"indices": [
						0,
						23
					]
				}]
			},
			"description": {
				"urls": []
			}
		},
		"protected": False,
		"followers_count": 6128663,
		"friends_count": 12,
		"listed_count": 12900,
		"created_at": "Wed May 23 06:01:13 +0000 2007",
		"favourites_count": 32,
		"utc_offset": None,
		"time_zone": None,
		"geo_enabled": None,
		"verified": True,
		"statuses_count": 3659,
		"lang": "None",
		"contributors_enabled": None,
		"is_translator": None,
		"is_translation_enabled": None,
		"profile_background_color": "None",
		"profile_background_image_url": "None",
		"profile_background_image_url_https": "None",
		"profile_background_tile": None,
		"profile_image_url": "None",
		"profile_image_url_https": "https://pbs.twimg.com/profile_images/942858479592554497/BbazLO9L_normal.jpg",
		"profile_banner_url": "https://pbs.twimg.com/profile_banners/6253282/1497491515",
		"profile_link_color": "None",
		"profile_sidebar_border_color": "None",
		"profile_sidebar_fill_color": "None",
		"profile_text_color": "None",
		"profile_use_background_image": None,
		"has_extended_profile": None,
		"default_profile": False,
		"default_profile_image": False,
		"following": None,
		"follow_request_sent": None,
		"notifications": None,
		"translator_type": "None"
	},
	"geo": None,
	"coordinates": None,
	"place": None,
	"contributors": None,
	"is_quote_status": False,
	"retweet_count": 161,
	"favorite_count": 296,
	"favorited": False,
	"retweeted": False,
	"possibly_sensitive": False,
	"possibly_sensitive_appealable": False,
	"lang": "en"
}


def _get_status():
    api =tweepy.API
    status = Status.parse(api,DATA)

    return status


def _get_search_response():
    rep = {
            "statuses": [
                _get_status()
            ],
            "search_metadata": {
            "completed_in": 0.057,
            "max_id": 0,
            "max_id_str": "0",
            "next_results": "?max_id=967574182522482687&q=nasa&include_entities=1&result_type=popular",
            "query": "nasa",
            "count": 3,
            "since_id": 0,
            "since_id_str": "0"
        }
    }
    return rep

@override_settings(
    SITE_ID=1,
    WHITENOISE_AUTOREFRESH=True,
    CELERY_ALWAYS_EAGER=True,
    CELERY_EAGER_PROPAGATES_EXCEPTIONS=True,
    STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage',
)
class InterrogatorTestCase(PolitikweetBaseTestCase, TransactionTestCase, ):

    def setUp(self):
        self.client = Client()
        self.rf = RequestFactory()

        self.cities = mommy.make(
            City, _quantity=50
        )

        self.user = User.objects.create_user("Interrogator", "interrogator@test.com")
        self.user.set_password("interrogateA@")

        self.team = mommy.make(Team)

        self.team.add_user(self.user)

        all_perms = Permission.objects.all()

        interrogator_auth_group, created = Group.objects.get_or_create(
            name="Interrogator"
        )

        librarian_auth_group, created  = Group.objects.get_or_create(
            name="Librarian"
        )

        interlocutor_auth_group, created  = Group.objects.get_or_create(
            name="Interlocutor"
        )

        supplicant_auth_group, created  = Group.objects.get_or_create(
            name="Supplicant"
        )

        librarian_auth_group.permissions.add(*all_perms)

        self.user.groups.add(interrogator_auth_group)
        self.user.groups.add(librarian_auth_group)
        self.user.groups.add(interlocutor_auth_group)
        self.user.groups.add(supplicant_auth_group)

        self.user.save()

        self.client.login(username='Interrogator', password='interrogateA@')

        self._social_app = mommy.make('SocialApp', provider="twitter")

        self.assertEqual(self.user.is_authenticated, True)

    def test_secure_interrogator_dashboard_table_loads(self):
        """
        test the secure dashboard can be reached
        """
        import random
        team = mommy.make(Team)
        city = mommy.make(City)
        interrogatory = mommy.make(
            Interrogation,
            team=team,
            city=city,
            user=self.user
        )
        mommy.make(
            InterrogationUnit,
            interrogation=interrogatory,
            is_complete=lambda: random.choice([True, False]),
            _quantity=100
        )

        response = self.client.get(
            reverse('secure-interrogator-dashboard'),
            data={
                'uuid': interrogatory.uuid,
            },
            follow=True)
        self.assertEqual(self.user.is_authenticated, True)
        self.assertEqual(response.status_code, 200)


    @patch.object(tweepy.API, 'get_status', return_value=_get_status(), autospec=True)
    def test_secure_interrogator_external_entity_detail_loads(self, mock_method):
        """
        interrogation unit detail
        """
        from interrogator.models import Interrogation, InterrogationUnit
        from django.utils.crypto import get_random_string
        import random

        team = mommy.make(Team)
        city = mommy.make(City)

        _social_account = mommy.make('SocialAccount', user=self.user)
        _social_tokens = mommy.make('SocialToken', app_id=self._social_app.id, account_id=_social_account.id)

        interrogatory = mommy.make(
            Interrogation,
            team=team,
            city=city,
            user=self.user
        )
        units = mommy.make(
            InterrogationUnit,
            external_entity_id=get_random_string(length=36),
            team=team,
            user=self.user,
            interrogation=interrogatory,
            is_complete=lambda: random.choice([True, False]),
            _quantity=100
        )

        unit = random.choice(units)

        response = self.client.get(
            reverse(
                'secure-interrogator-external-entity-id-detail',
                kwargs={
                    'uuid': unit.uuid,
                    'external_entity_id': unit.external_entity_id
                },
            ),
            follow=True
        )
        self.assertEqual(self.user.is_authenticated, True)
        mock_method.assert_called_once()
        self.assertEqual(response.status_code, 200)


    def test_secure_interrogator_dashboard_loads(self):
        """
        interrogation table loads
        """
        response = self.client.get(reverse('secure-interrogator-dashboard'), follow=True)
        self.assertEqual(self.user.is_authenticated, True)
        self.assertEqual(response.status_code, 200)


    def test_secure_interrogator_dashboard_as_view(self):
        from interrogator.views import InterrogatorListView
        request = self.rf.get(reverse('secure-interrogator-dashboard'))
        setattr(request, "user", self.user)
        response = InterrogatorListView.as_view()(request)
        self.assertEqual(self.user.is_authenticated, True)
        self.assertEqual(response.status_code, 200)


    def test_secure_interrogator_form(self):
        response = self.client.get(reverse('secure-interrogator-define'), follow=True)
        self.assertEqual(response.status_code, 200)


    def test_secure_interrogator_form_post(self):
        """
        define interrogation
        """
        from core.models import City
        from interrogator.models import Interrogation
        from interrogator.views import CreateInterrogatorView

        import random
        import string
        from faker import Faker

        from time import sleep

        fake = Faker()
        Faker.seed(0)
        city = City.objects.all().order_by('?').first()
        sample_size = 1000
        N = 10
        key = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))

        self.client.login(username='Interrogator', password='interrogateA@')

        form_data = {
            'keyword': key,
            'city': city.id,
            'team': self.team.id,
            'user': self.user.id,
            'sample_size': sample_size
        }

        request = self.rf.post(
            reverse('secure-interrogator-define'),
            post_data=form_data,
        )
        setattr(request, 'user', self.user)  # team_id
        setattr(request, 'team', self.team)

        response = CreateInterrogatorView.as_view()(request)

        self.assertIsNotNone(response.context_data)
        self.assertEqual(response.status_code, 200)


    @patch.object(tweepy.API, 'search_tweets', return_value=_get_search_response(), autospec=True)
    def test_interrogate_celery_task(self, mock_method):
        from interrogator.tasks import interrogate
        from interrogator.models import Interrogation
        import random
        team = mommy.make(Team)
        city = mommy.make(City)


        _social_account = mommy.make('SocialAccount', user=self.user)
        _social_tokens = mommy.make('SocialToken', app_id=self._social_app.id, account_id=_social_account.id)

        interrogatory = mommy.make(Interrogation, team=team, city=city, user=self.user)

        units=mommy.make(
            InterrogationUnit,
            interrogation=interrogatory,
            is_complete=lambda: random.choice([True, False]),
            _quantity=100
        )
        task = interrogate.apply()

        self.assertIsNotNone(task)
        mock_method.assert_called_once()
        self.assertEqual(interrogatory.interrogation_units.all().count(), 100)


    def test_extend_tendril_celery_task(self):
        from interrogator.tasks import extend_tendril

        team = mommy.make(Team)
        self.city = mommy.make(City)
        self.interrogatory = mommy.make(Interrogation, user=self.user)

        task = extend_tendril.apply(
            (self.interrogatory.id, self.user.id, team.id)
        )
        self.assertEqual(task.state, 'SUCCESS')


    def test_inquisitor_celery_task(self):
        """
        test inquisitor task
        """
        from interrogator.tasks import inquisitor
        import random
        interrogatory = mommy.make(Interrogation, team=self.team, city=self.cities.pop(), user=self.user)
        mommy.make(
            InterrogationUnit,
            interrogation=interrogatory,
            is_complete=lambda: random.choice([True, False]),
            _quantity=1000
        )
        task = inquisitor.apply(args=[interrogatory.pk])
        self.assertEqual(task.state, 'SUCCESS')


    def test_interrogation_raise_flag(self):
        """
        test task raise flag
        """
        from interrogator.models import Interrogation, InterrogationUnit
        from core.models import City, Team


        _social_account = mommy.make('SocialAccount', user=self.user)
        _social_tokens = mommy.make('SocialToken', app=self._social_app, account=_social_account)

        team = mommy.make(Team)
        city = mommy.make(City)
        interrogatory = mommy.make(
            Interrogation,
            user=self.user,
            city=city,
            team=team,
        )
        interrogatory.is_complete = True
        interrogatory.is_locked = True
        interrogatory.save()

        interrogatory.refresh_from_db()

        mommy.make(
            InterrogationUnit,
            interrogation=interrogatory,
            is_complete=True,
            _quantity=100
        )

        interrogatory.check_for_complete_raise_flag()
        interrogatory.refresh_from_db()

        self.assertEqual(interrogatory.is_complete, True)
        self.assertEqual(interrogatory.is_locked, True)


    def tearDown(self):
        pass  # passself.celery_worker.__exit__(None, None, None)
