#!/bin/sh
set -e
if [ "$1" = 'web' ]; then
    sleep 30
    echo "***************************COLLECT STATIC"
    python /politikweet/manage.py collectstatic --noinput
    echo  "***************************make=migrations"
    python /politikweet/manage.py makemigrations --noinput
    echo "***************************migrate "
    python /politikweet/manage.py migrate
    echo "***************************run server"
    python /politikweet/manage.py runserver 0.0.0.0:8500
  exec "$@"
fi
if [ "$1" = 'celery' ]; then
  echo "this is the celery rthing"

  exec "$@"
fi
if [ "$1" = 'mongo' ]; then
  exec "$@"
fi

if [ "$1" = 'rabbitmq' ]; then
  exec "$@"
fi

if [ "$1" = 'db' ]; then
  set -e
  echo ">>> STARTUP POSTGRESQL ENTRYPOINT..."
  wait ${!}

  EXIT_CODE=$?

  echo ">>> POSTGRESQL TERMINATED WITH EXIT CODE: $EXIT_CODE"

  exit $EXIT_CODE
  exec "$@"
fi

exec "$@"




