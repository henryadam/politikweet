from __future__ import absolute_import

from politikweet.celery import app
from politikweet.tasks import PolitikweetBaseTask
from politikweet.utils import random_wobble


@app.task(bind=True, base=PolitikweetBaseTask, countdown=random_wobble(), routing_key='celery')
def public_task( self, *args, **kwargs ):
    print(self.request)