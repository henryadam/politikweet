from __future__ import unicode_literals

import datetime
import math
import random
from collections import OrderedDict

import plotly
import plotly.graph_objs as go
import pytz
from django.conf import settings
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic import TemplateView, View
from mongoengine.queryset.visitor import Q as _Q

from core.nosql.documents import Sentimentalite, KeywordAnalysisUnit, SentimentAccumulator
from politikweet.mixins import JSONResponseMixin
from politikweet.utils import normalize

TRACKED_KEYWORDS_LEFT = [
    "president",
    "congress",
    "senate",
    "white house",
    "supreme court",
    "politics",
    "political",
    "demagogue",
]
TRACKED_KEYWORDS_RIGHT = [
    "house of representatives",
    "Department of Agriculture",
    "Department of Commerce",
    "Department of Defense",
    "Department of Education",
    "Department of Health and Human Services",
    "Department of Homeland Security",
    "Department of Housing and Urban Development",
    "Department of Energy"]

TRACKED_KEYWORDS = TRACKED_KEYWORDS_LEFT + TRACKED_KEYWORDS_RIGHT
class HomeView(TemplateView):
    template_name = "public/home.html"


    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['tracking'] = TRACKED_KEYWORDS

        keywords = KeywordAnalysisUnit.objects.filter(
            value__gt=settings.MINIMUM_KEYWORD_FREQUENCY_TO_APPEAR
        ).order_by('-value')

        results = []
        upper_limit_actual = keywords[0].value if keywords else 0
        lower_limit_actual = keywords[len(keywords) - 1].value if keywords else 0
        if upper_limit_actual and lower_limit_actual:
            divisor = random.choice([10, 11, 12])
            bounds = {
                'actual': {
                    'lower': round(math.sqrt(lower_limit_actual), 4),
                    'upper': round(math.sqrt(upper_limit_actual), 4)
                },
                'desired': {
                    'lower': 1, 'upper': 6
                }
            }
            for keyword in keywords:
                payload = {}
                payload['key'] = keyword.key
                payload['sentiment'] = keyword.sentiment
                payload['value'] = round(math.sqrt(keyword.value), 4)
                results.append(payload)

            normalized = normalize([i['value'] for i in results], bounds)

            normalized_results = []

            for result, normal in zip(results, normalized):
                normalized_payload = {}
                normalized_payload['key'] = result["key"]
                normalized_payload['sentiment'] = round(result["sentiment"], 4)
                normalized_payload['value'] = round(normal, 4)
                normalized_results.append(normalized_payload)

            _results_= normalized_results

            context['tag_cloud_results'] = _results_
        else:
            context['tag_cloud_results'] = []

        return context


class AboutView(TemplateView):
    template_name = "public/about.html"


    def get_context_data(self, **kwargs):
        context = super(AboutView, self).get_context_data(**kwargs)
        context['tracking_left'] = TRACKED_KEYWORDS_LEFT
        context['tracking_right'] = TRACKED_KEYWORDS_RIGHT
        return context


class PrivacyView(TemplateView):
    template_name = "public/privacy.html"


class TermsView(TemplateView):
    template_name = "public/terms.html"


class TagCloudView(TemplateView):
    template_name = "public/tag_cloud.html"


    def get_context_data(self, **kwargs):
        context = super(TagCloudView, self).get_context_data(**kwargs)
        keywords = KeywordAnalysisUnit.objects.filter(
            value__gt=settings.MINIMUM_KEYWORD_FREQUENCY_TO_APPEAR
        ).order_by('-value')

        results = []
        upper_limit_actual = keywords[0].value if keywords else 0
        lower_limit_actual = keywords[len(keywords) - 1].value if keywords else 0
        if upper_limit_actual and lower_limit_actual:
            divisor = random.choice([10, 11, 12])
            bounds = {
                'actual': {
                    'lower': round(math.sqrt(lower_limit_actual), 4),
                    'upper': round(math.sqrt(upper_limit_actual), 4)
                },
                'desired': {
                    'lower': 1, 'upper': 6
                }
            }
            for keyword in keywords:
                payload = {}
                payload['key'] = keyword.key
                payload['sentiment'] = keyword.sentiment
                payload['value'] = round(math.sqrt(keyword.value), 4)
                results.append(payload)

            normalized = normalize([i['value'] for i in results], bounds)

            normalized_results = []

            for result, normal in zip(results, normalized):
                normalized_payload = {}
                normalized_payload['key'] = result["key"]
                normalized_payload['sentiment'] = round(result["sentiment"], 4)
                normalized_payload['value'] = round(normal, 4)
                normalized_results.append(normalized_payload)

            random.shuffle(normalized_results)

            context['tag_cloud_results'] = normalized_results
        else:
            context['tag_cloud_results'] = []
        return context


class TagCloudJSONView(View):
    def get(self, request, *kwargs):
        import time
        start_time = time.time()
        lower_limit = datetime.datetime.utcnow() - datetime.timedelta(days=90)
        upper_limit = datetime.datetime.utcnow()
        print(lower_limit, " ", upper_limit)
        keywords = Sentimentalite.objects.filter(
            (_Q(key_words__startswith="@") | _Q(key_words__startswith="#")) &
            (_Q(created_date__gte=lower_limit) & _Q(created_date__lte=upper_limit))
        ).order_by('created_date')[:250]

        keywords_frequency = keywords.item_frequencies(field='key_words', normalize=False, map_reduce=True)

        filtered_sorted_keywords = OrderedDict({
            key: value for (key, value) in sorted(keywords_frequency.items(), reverse=True, key=lambda item: item[1])
            if value > 3 and len(key) > 3 and (key.startswith('#') or key.startswith('@'))
        })

        to_save = []
        for key, value in filtered_sorted_keywords.items():
            payload = KeywordAnalysisUnit()
            payload['key'] = key
            payload['value'] = float(value)
            sentimentalites = Sentimentalite.objects.filter(
                _Q(key_words=key) & _Q(created_date__gte=lower_limit) & _Q(created_date__lte=upper_limit))
            _keyword_avg_sentiment_ = sum([
                item.to_mongo().get('sentiment_score', {}).get('compound', {}) for item in (sentimentalites)
            ]) / len(sentimentalites)
            payload['sentiment'] = _keyword_avg_sentiment_

            to_save.append(payload)

        KeywordAnalysisUnit.drop_collection()
        KeywordAnalysisUnit.objects.insert(to_save)

        duration = time.time() - start_time

        return JsonResponse({"success": True, "duration": duration, "keywords": len(to_save)})


class TagCloudDetailView(TemplateView):
    template_name = "public/tag_cloud_detail.html"


    @method_decorator(ensure_csrf_cookie)
    def dispatch(self, *args, **kwargs):
        return super(TagCloudDetailView, self).dispatch(*args, **kwargs)


    def get_context_data(self, **kwargs):
        context = super(TagCloudDetailView, self).get_context_data(**kwargs)
        term = kwargs.get('search_term')

        context['term'] = term
        details = Sentimentalite.objects.filter(
            key_words__icontains=term,
            created_date__gt=datetime.datetime.utcnow().replace(tzinfo=pytz.UTC) + datetime.timedelta(days=7 * -1)
        )

        sentiment = [item['embedded_url_payload'].get('avg_analysis', {}).get('compound', 0) for item in details]
        sentiment_length = len(sentiment) or 1
        sentiment = round(sum(sentiment) / sentiment_length, 4)

        urls = []
        for_dedup = []
        for detail in details:
            if detail['embedded_url_payload'].get('url') and not detail['embedded_url_payload'].get('url') in for_dedup:
                for_dedup.append(detail['embedded_url_payload']['url'])
                urls.append((
                    detail['embedded_url_payload']['url'],
                    round(detail['embedded_url_payload']['avg_analysis']['compound'], 4),
                    detail['embedded_url_payload']['title'],
                    detail['id']
                ))

        context['details'] = details
        context['urls'] = set(urls)
        context['sentiment'] = sentiment

        return context


class PolitikalSentiment(TemplateView):
    template_name = "public/politikal_sentiment.html"


    def get_context_data(self, **kwargs):
        context = super(PolitikalSentiment, self).get_context_data(**kwargs)
        data = SentimentAccumulator.objects.all().order_by('date')
        context['tracking_left'] = TRACKED_KEYWORDS_LEFT
        context['tracking_right'] = TRACKED_KEYWORDS_RIGHT
        data = [go.Scatter(
            name='sentiment_polarity_trends',
            mode='lines+markers',
            line=dict(
                color='#fcfc00',
                width=2,
                smoothing=1,
                simplify=True,
                shape="hvh"

            ),
            x=[i.date for i in data],

            y=[i.value for i in data],

        )]

        layout = go.Layout(
            showlegend=False,
            autosize=True,
            plot_bgcolor="rgba(0,0,0,0)",
            paper_bgcolor="rgba(0,0,0,0)",
            title='Sentiment Polarity Trend',
            font=dict(family='Nova Mono', size=24, color='#ffffff'),
            xaxis=dict(
                showgrid=True,
                zeroline=False,
                showline=True,
                title='Date',
                linecolor='#ffffff',
                titlefont=dict(
                    size=14,
                    color='#ffffff'
                ),
                tickfont=dict(
                    size=12,
                    color='#ffffff'
                )
            ),
            yaxis=dict(
                showgrid=True,
                zeroline=True,
                zerolinecolor='#0bff0b',
                showline=True,
                range=[-1, 1],
                title='Polarity',
                linecolor='#ffffff',
                titlefont=dict(
                    size=16,
                    color='#ffffff'
                ),
                tickfont=dict(
                    size=14,
                    color='#ffffff'
                )
            ),

        )

        fig = go.Figure(data=data, layout=layout)
        plot_div = plotly.offline.plot(fig, output_type='div')

        context['graph'] = plot_div

        context['test'] = len(data)
        return context


class TagCloudUrlDetailsView(TemplateView, JSONResponseMixin):

    def post(self, request, **kwargs):

        print(kwargs)

        id = kwargs.pop('search_term')

        response_status = 400
        response = {}

        try:
            item = Sentimentalite.objects.get(id=id)
            data = item.sentences_score
            keywords = item.key_words
            title = item.embedded_url_payload['title']
            link = item.embedded_url_payload['url']
            response = {
                "data": {
                    "x": [idx + 1 for idx, i in enumerate(data)],
                    "y": [i['compound'] for i in data]
                },
                "title": title,
                "link": link,
                "keywords": keywords
            }

            response_status = 200


        except Exception as e:
            response = {
                "error": e
            }

        finally:
            return JsonResponse(response, status=response_status)
