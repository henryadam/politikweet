"""politikweet URL Configuration"""
from __future__ import absolute_import, unicode_literals, print_function
from django.conf import settings
from django.conf.urls import include, url, re_path
from django.conf.urls import url
from public.views import HomeView, \
    TagCloudView, \
    TagCloudDetailView, \
    TagCloudJSONView, \
    PolitikalSentiment, \
    TagCloudUrlDetailsView, \
    AboutView, \
    PrivacyView, \
    TermsView

urlpatterns = [
    re_path(r'^$', HomeView.as_view(), name='home'),

]
