"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.contrib.auth.models import User
from django.urls import reverse
from django.test import TestCase, override_settings
from faker import Faker

fake = Faker()


@override_settings(
        CELERY_ALWAYS_EAGER=True,
        CELERY_EAGER_PROPAGATES_EXCEPTIONS=True,
        SITE_ID=1)
class PublicTestCase(TestCase):

    def setUp(self):


        self.user = User.objects.create_user("test.user.1", 'test@test.com', 'qwerty')
        self.user.first_name = fake.first_name()
        self.user.last_name = fake.last_name()
        self.user.save()

    def test_public_register_loads(self):
        response = self.client.get(reverse('account_signup'), follow=True)
        self.assertEqual(response.status_code, 200)

    def test_public_views_home_loads(self):
        response = self.client.get(reverse('home'), follow=True)
        self.assertEqual(response.status_code, 200)

    def test_public_views_about_loads(self):
        response = self.client.get(reverse('about'), follow=True)
        self.assertEqual(response.status_code, 200)

    def test_public_tag_cloud_view(self):
        response = self.client.get(reverse('tag-cloud'), follow=True)
        self.assertEqual(response.status_code, 200)

    def test_public_tag_cloud_detail_view(self):
        response = self.client.get(reverse('tag-cloud-term-detail', kwargs={"search_term":"@CNN"}), follow=True)
        self.assertEqual(response.status_code, 200)

    def test_public_politikal_sentiment_view(self):
        response = self.client.get(reverse('politikal-sentiment'), follow=True)
        self.assertEqual(response.status_code, 200)



