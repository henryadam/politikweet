$(document).ready(function () {

    $("#tag-cloud-detail-modal").on("shown.bs.modal", function (e) {

        var link = e.relatedTarget;

        var item = $(link).attr('data-item');

        var target = $(this).find('.modal-body');


        var target_url = "/tag-cloud-url-detail/" + item + "/?_=" + moment();

        var data;

        function articleChart(x, y, title, link, keywords) {

            target.find('#graph-title').remove();
            target.find('#graph').remove();
            target.find('#graph-keywords').remove();


            target.append($("<div id='graph-title'></div>"));

            target.append($("<div id='graph'></div>"));

            target.append($("<div id='graph-keywords'><h5>Matched Keywords</h5></div>"));

            title_target = target.find('#graph-title');
            keyword_target = target.find('#graph-keywords');

            title_link = "<p><small><a class='btn btn-sm btn-primary social' target='_blank' href='" + link + "'>Visit Original Article</a></small></p>";

            title_target.append($("<h5>" + title + '<p>' + title_link + '</p>' + "</h5>"));

            keywords_list = $("<ul id='key-words' class='list-inline lrg-pill'></ul>");

            keyword_target.append(keywords_list);

            $.each(keywords, function (e, data) {

                list_item = $('<li><h3><span class="label label-default">' + data + '</span></h3></li>');
                $('#key-words').append(list_item)

            });

            var data = [{
                x: x,
                y: y,
                line: {
                    color: '#ffcc00',
                    width: 1,
                    line: {shape: 'vh'}
                },
                mode: 'lines+markers',
                type: 'scatter',

            }];

            var layout = {
                title: 'Sentiment Flow Through Article',
                paper_bgcolor: "rgba(0,0,0,0)",
                plot_bgcolor: "rgba(0,0,0,0)",
                yaxis: {
                    showgrid: true,
                    showline: true,
                    zerolinecolor: '#1fff0c',
                    zerolinewidth: 1,
                    title: "Sentiment Polarity",
                    range: [-1, 1],
                    color: "#ffffff"
                },
                xaxis: {
                    showgrid: true,
                    zeroline: true,
                    showline: true,
                    title: "Sentence Number",
                    color: "#ffffff",


                },
                titlefont: {
                    family: 'Droid Sans',
                    size: 18,
                    color: '#ffffff'
                }

            };

            var d3 = Plotly.d3;

            var WIDTH_IN_PERCENT_OF_PARENT = 99,
                HEIGHT_IN_PERCENT_OF_PARENT = 50;

            var gd3 = d3.select('#graph')
                .append('div')
                .style({
                    width: WIDTH_IN_PERCENT_OF_PARENT + '%',
                    'margin-left': (100 - WIDTH_IN_PERCENT_OF_PARENT) / 2 + '%',

                    height: HEIGHT_IN_PERCENT_OF_PARENT + 'vh',
                    'margin-top': (100 - HEIGHT_IN_PERCENT_OF_PARENT) / 10 + 'vh'
                });

            var gd = gd3.node();

            Plotly.plot(gd, data, layout);

            window.onresize = function () {

                Plotly.Plots.resize(gd);
            };


        }

        $.ajax({
            "type": "POST",
            "dataType": "json",
            "cache": false,
            "url": target_url,
            "success": function (result) {
                $('#graph-title').remove();
                $('#graph').remove();
                $('#graph-keywords').remove();
                articleChart(
                    result['data']['x'],
                    result['data']['y'],
                    result['title'],
                    result['link'],
                    result['keywords'])

            }
        });


    });

});